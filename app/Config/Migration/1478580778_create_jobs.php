<?php
class CreateJobs extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = 'create_jobs';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'jobs' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
					'job_title' => array('type' => 'string', 'null' => false, 'default' => null),
					'slug' => array('type' => 'string', 'null' => false, 'default' => null),
					'company_id' => array('type' => 'integer', 'null' => false, 'default' => null),
					'location' => array('type' => 'string', 'null' => false, 'default' => null),
					'type' => array('type' => 'integer', 'null' => true, 'default' => 0),
					'category_id' => array('type' => 'string', 'null' => true, 'default' => null),
					'salary' => array('type' => 'integer', 'null' => false, 'default' => null),
					'available_to' => array('type' => 'datetime', 'null' => false, 'default' => null),
					'job_group_id' => array('type' => 'string', 'null' => true, 'default' => null),
					'job_description' => array('type' => 'text', 'null' => false, 'default' => null),
					'benefit' => array('type' => 'text', 'null' => false, 'default' => null),
					'required' => array('type' => 'text', 'null' => false, 'default' => null),
					'is_hot' => array('type' => 'integer', 'null' => false, 'default' => null),
					'hot_to' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'is_urgent' => array('type' => 'integer', 'null' => false, 'default' => null),
					'urgent_to' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'is_new' => array('type' => 'integer', 'null' => false, 'default' => null),
					'new_to' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'viewed' => array('type' => 'integer', 'null' => true, 'default' => 0),
					'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
					'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => true),
					),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'jobs'
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function after($direction) {
		return true;
	}
}
