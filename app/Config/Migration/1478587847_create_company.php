<?php
class CreateCompany extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = 'create_company';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'companies' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
					'company_name' => array('type' => 'string', 'null' => false, 'default' => null),
					'description' => array('type' => 'text', 'null' => false, 'default' => null),
					'career_id' => array('type' => 'integer', 'null' => false, 'default' => null),
					'slug' => array('type' => 'string', 'null' => false, 'default' => null),
					'status' => array('type' => 'integer', 'null' => false, 'default' => null),
					'logo_url' => array('type' => 'string', 'null' => false, 'default' => null),
					'homepage_url' => array('type' => 'string', 'null' => false, 'default' => null),
					'email' => array('type' => 'string', 'null' => true, 'default' => null),
					'service_type' => array('type' => 'integer', 'null' => false, 'default' => null),
					'service_available_to' => array('type' => 'datetime', 'null' => false, 'default' => null),
					'telephone' => array('type' => 'string', 'null' => false, 'default' => null),
					'location' => array('type' => 'string', 'null' => false, 'default' => null),
					'facebook_url' => array('type' => 'string', 'null' => true, 'default' => null),
					'twitter_url' => array('type' => 'string', 'null' => true, 'default' => null),
					'jobs' => array('type' => 'integer', 'null' => false, 'default' => 0),
					'employees' => array('type' => 'integer', 'null' => false, 'default' => 0),
					'followers' => array('type' => 'integer', 'null' => false, 'default' => 0),
					'rating' => array('type' => 'string', 'null' => true, 'default' => null),
					'rating_count' => array('type' => 'integer', 'null' => false, 'default' => 0),
					'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
					'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => true),
					),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'companies'
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function after($direction) {
		return true;
	}
}
