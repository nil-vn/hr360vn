<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/:language/:controller/:action/*',array(),array('language' => 'eng|vi'));

	Router::connect('/:language/:controller',array('action' => 'index'),array('language' => 'eng|jp'));

	Router::connect('/:language', array('controller' => 'home', 'action' => 'index'),array('language' => 'eng|vi'));

	Router::connect('/', array('controller' => 'home', 'action' => 'index'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	// Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	Router::connect('/admin/category', array('controller' => 'AdminCategory', 'action' => 'index'));
	Router::connect('/admin/category/add', array('controller' => 'AdminCategory', 'action' => 'postNewCategory'));
	Router::connect('/admin/category/edit/:id', array('controller' => 'AdminCategory', 'action' => 'updateCategory'), array('id' => '[0-9]+'));
	Router::connect('/admin/category/delete', array('controller' => 'AdminCategory', 'action' => 'deleteCategories'));
	Router::connect('/admin/jobs', array('controller' => 'AdminJob', 'action' => 'index'));
	Router::connect('/admin/jobs/add', array('controller' => 'AdminJob', 'action' => 'postNewJob'));
	Router::connect('/admin/jobs/edit/:id', array('controller' => 'AdminJob', 'action' => 'updateJob'), array('id' => '[0-9]+'));
	Router::connect('/admin/jobs/delete', array('controller' => 'AdminJob', 'action' => 'deleteJobs'));
	Router::connect('/admin/company', array('controller' => 'AdminCompany', 'action' => 'index'));
	Router::connect('/admin/company/add', array('controller' => 'AdminCompany', 'action' => 'postNewCompany'));
	Router::connect('/admin/company/edit/:id', array('controller' => 'AdminCompany', 'action' => 'updateCompany'), array('id' => '[0-9]+'));
	Router::connect('/admin/company/delete', array('controller' => 'AdminCompany', 'action' => 'deleteCompany'));
	Router::connect('/admin/company/:id', array('controller' => 'AdminCompany', 'action' => 'getCompany'), array('id' => '[0-9]+'));

	Router::connect('/companies', array('controller' => 'Company', 'action' => 'getCompanies'));
	Router::connect('/company/:slug', array('controller' => 'Company', 'action' => 'getCompanyDetail'), array('slug' => '[a-zA-Z-]+'));
	Router::connect('/jobs', array('controller' => 'Jobs', 'action' => 'getJobs'));
	Router::connect('/jobs/:slug', array('controller' => 'Jobs', 'action' => 'getJobDetail'), array('slug' => '[a-zA-Z-]+'));

	Router::connect('/candidates', array('controller' => 'Candidates', 'action' => 'getCandidates'));
	Router::connect('/candidate/:name', array('controller' => 'Candidates', 'action' => 'getCandidate'), array('slug' => '[a-zA-Z-]+'));

	Router::connect('/login', array('controller' => 'Login', 'action' => 'getLogin'));
	Router::connect('/registration', array('controller' => 'Login', 'action' => 'postRegistration'));
	Router::connect('/create-resume', array('controller' => 'Candidates', 'action' => 'postResume'));
	Router::connect('/search', array('controller' => 'Home', 'action' => 'getSearch'));
	// Router::connect('/404', array('controller' => 'Errors', 'action' => 'error404'));
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
