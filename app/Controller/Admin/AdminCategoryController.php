<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AdminController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AdminCategoryController extends AdminController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('logout', 'login');
    }

    /**
     * This controller does not use a model
     *
     * @var array
     */
    //public $uses = array('Report');

    /**
     * $helpers Helpers list for Adminpage, using view
     * @var array
     */
    //public $helpers = array('Menu');

    /**
     * Upload file csv component
     * @var folder, file, child folder
     */
    //public $components = array('UploadFile');

    /**
     * Set layout for admin page
     */
    public $layout = 'admin';

    /**
     * Index action
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *  or MissingViewException in debug mode.
     */
    public function index(){
        $this->loadModel('Category');
        $cats = $this->Category->find('all', array('order' => array('Category.name ASC')));
        $this->set('cats', $cats);
        $this->set('controller_title', 'Job Category');
    }

    public function postNewCategory(){
        $this->loadModel('Category');
        if($this->request->is('post')){
            $category = $this->request->data;
            $category['slug'] = strtolower(Inflector::slug($category['name'], $replacement = '-'));
            try{
                $this->Category->save($category);

            } catch(Exception $e) {
                $this->Session->setFlash(__($e->getMessage()), 'default', array(), 'error');
            }
        } else {
            $this->Session->setFlash(__('Method not allowed', 'default', array(), 'error'));
        }
        return $this->redirect('/admin/category');
    }

    public function updateCategory(){
        $this->loadModel('Category');
        $category   = $this->Category->findById($this->request->id);
        $cats       = $this->Category->find('all', array('order' => array('Category.name ASC')));

        if($this->request->is('post')){
            $data = $this->request->data;
            $data['slug'] = strtolower(Inflector::slug($data['name'], $replacement = '-'));
            $this->Category->id = $this->request->id;
            try{
                $this->Category->save($data);
                $this->Session->setFlash(__('Updated success!'), 'default', array(), 'success');
            } catch(Exception $e) {
                $this->Session->setFlash(__($e->getMessage()), 'default', array(), 'error');
            }
            return $this->redirect('/admin/category');
        }
        $this->set(compact('category'));
        $this->set('cats', $cats);
        $this->set('controller_title', 'Edit Category');
    }

    public function deleteCategories(){
        if($this->request->is('post')){
            $cats = $this->request->data['category_id'];
            // debug($cats);exit();
            try{
                foreach ($cats as $key => $value) {
                    $this->Category->delete($value);
                }
                $this->Session->setFlash('Delete success!', 'default', array(), 'success');
            } catch(Exception $e) {
                $this->Session->setFlash(__($e->getMessage()), 'default', array(), 'error');
            }
        } else {
            $this->Session->setFlash('Method not allowed', 'default', array(), 'error');
        }
        return $this->redirect(array('controller' => 'admin', 'action' => 'getSettingCategory'));
    }
}
