<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AdminController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AdminCompanyController extends AdminController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('logout', 'login');
    }

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Company');

    /**
     * $helpers Helpers list for Adminpage, using view
     * @var array
     */
    //public $helpers = array('Menu');

    /**
     * Upload file csv component
     * @var folder, file, child folder
     */
    //public $components = array('UploadFile');

    /**
     * Set layout for admin page
     */
    public $layout = 'admin';

    /**
     * Index action
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *  or MissingViewException in debug mode.
     */
    public function index(){
        $this->loadModel('Company');
        $companies = $this->Company->find('all', array('order' => array('Company.company_name ASC')));
        $this->set(compact('companies'));
        $this->set('controller_title', 'Company list');
    }

    public function postNewCompany(){
        $this->layout = false;
        $this->loadModel('Company');
        if($this->request->is('post')){
            $company = $this->request->data;
            $company['slug'] = strtolower(Inflector::slug($company['company_name'], $replacement = '-'));
            $company['service_available_to'] = date('Y-m-d h:i:s', time());
            try{
                $this->Company->save($company);

            } catch(Exception $e) {
                $this->Session->setFlash(__($e->getMessage()), 'default', array(), 'error');
            }
        } else {
            $this->Session->setFlash(__('Method not allowed', 'default', array(), 'error'));
        }
        return $this->redirect('/admin/company');
    }

    public function updateCompany(){
        $company   = $this->Company->findById($this->request->id);

        if($this->request->is('post')){
            $data = $this->request->data;
            $data['id'] = $this->request->id;
            $data['slug'] = strtolower(Inflector::slug($data['company_name'], $replacement = '-'));
            $data['service_available_to'] = date('Y-m-d h:i:s', time());
            try{
                $this->Company->set($data);
                $this->Company->save();
                $this->Session->setFlash(__('Save success!', 'default', array(), 'success'));
            } catch(Exception $e) {
                $this->Session->setFlash(__($e->getMessage()), 'default', array(), 'error');
            }
            //$company   = $this->Company->findById($this->request->id);
            $this->redirect('/admin/company');
        }
        $this->set(compact('company'));
    }

    public function deleteCompany(){
        if($this->request->is('post')){
            $cats = $this->request->data['category_id'];
            // debug($cats);exit();
            try{
                foreach ($cats as $key => $value) {
                    $this->Category->delete($value);
                }
                $this->Session->setFlash('Delete success!', 'default', array(), 'success');
            } catch(Exception $e) {
                $this->Session->setFlash(__($e->getMessage()), 'default', array(), 'error');
            }
        } else {
            $this->Session->setFlash('Method not allowed', 'default', array(), 'error');
        }
        return $this->redirect(array('controller' => 'admin', 'action' => 'getSettingCategory'));
    }

    public function getCompany(){
        $company_id = $this->request->id;
        $company = $this->Company->findById($company_id);
        $this->set(compact('company'));
    }
}
