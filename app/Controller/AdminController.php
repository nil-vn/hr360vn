<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AdminController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('logout', 'login');
    }

    /**
     * This controller does not use a model
     *
     * @var array
     */
    //public $uses = array('Report');

    /**
     * $helpers Helpers list for Adminpage, using view
     * @var array
     */
    //public $helpers = array('Menu');

    /**
     * Upload file csv component
     * @var folder, file, child folder
     */
    //public $components = array('UploadFile');

    /**
     * Set layout for admin page
     */
    public $layout = 'admin';

    /**
     * Index action
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *  or MissingViewException in debug mode.
     */
    public function index() {
        // debug(1);exit()
        $this->set('controller_title', 'Admin cPanel');
    }

    public function login() {
        $this->layout = 'login';
        if(!$this->Auth->user()){
            if ($this->request->is('post')) {
                if ($this->Auth->login()) {
                    if ($this->request->data['User']['remember_me'] == 'on') {
                        // remove "remember me checkbox"
                        unset($this->request->data['User']['remember_me']);

                        // hash the user's password
                        $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);

                        // write the cookie
                        $this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
                    }
                    return $this->redirect($this->Auth->redirectUrl());
                }
                $this->Flash->error(__('Invalid username or password, try again'));
            }
        } else {
            return $this->redirect($this->Auth->redirectUrl());
        }
    }

    public function logout() {
        // clear the cookie (if it exists) when logging out
        $this->Cookie->delete('remember_me_cookie');
        return $this->redirect($this->Auth->logout());
    }

    // public function getSettingCategory(){
    //     $this->loadModel('Category');
    //     $cats = $this->Category->find('all');
    //     $this->set('cats', $cats);
    //     $this->set('controller_title', 'Setting');
    // }

    // public function postNewCategory(){
    //     $this->loadModel('Category');
    //     if($this->request->is('post')){
    //         $category = $this->request->data;
    //         try{
    //             $this->Category->save($category);
    //         } catch(Exception $e) {
    //             $this->Session->setFlash(__($e->getMessage()), 'default', array(), 'error');
    //         }
    //     } else {
    //         $this->Session->setFlash(__('Method not allowed', 'default', array(), 'error'));
    //     }
    //     return $this->redirect(array('controller' => 'admin', 'action' => 'getSettingCategory'));
    // }

    // public function updateCategory(){
    //     $this->loadModel('Category');
    //     $category   = $this->Category->findById($this->request->id);
    //     $cats       = $this->Category->find('all');

    //     if($this->request->is('post')){
    //         $data = $this->request->data;
    //         $data['slug'] = Inflector::slug($data['name'], $replacement = '-');
    //         $this->Category->id = $this->request->id;
    //         try{
    //             $this->Category->save($this->request->data);
    //             $this->Session->setFlash(__('Updated success!'), 'default', array(), 'success');
    //         } catch(Exception $e) {
    //             $this->Session->setFlash(__($e->getMessage()), 'default', array(), 'error');
    //         }
    //         return $this->redirect(array('controller' => 'admin', 'action' => 'getSettingCategory'));
    //     }
    //     $this->set(compact('category'));
    //     $this->set('cats', $cats);
    //     $this->set('controller_title', 'Edit Category');
    // }

    // public function deleteCategories(){
    //     if($this->request->is('post')){
    //         $cats = $this->request->data['category_id'];
    //         // debug($cats);exit();
    //         try{
    //             foreach ($cats as $key => $value) {
    //                 $this->Category->delete($value);
    //             }
    //             $this->Session->setFlash('Delete success!', 'default', array(), 'success');
    //         } catch(Exception $e) {
    //             $this->Session->setFlash(__($e->getMessage()), 'default', array(), 'error');
    //         }
    //     } else {
    //         $this->Session->setFlash('Method not allowed', 'default', array(), 'error');
    //     }
    //     return $this->redirect(array('controller' => 'admin', 'action' => 'getSettingCategory'));
    // }

    // public function getDocument(){
    //     $category = $this->Category->findBySlug($this->request->slug);
    //     // debug($category);
    //     $this->set(compact('category'));
    // }
}
