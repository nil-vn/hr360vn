<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $user = false;
	public $components = array(
		'Flash',
		'Auth' => array(
			'loginRedirect' => array(
				'controller' => 'admin',
				'action' => 'index'
			),
			'logoutRedirect' => array(
				'controller' => 'admin',
					'action' => 'index'
			),
			'authenticate' => array(
				'Form' => array(
					'passwordHasher' => 'Blowfish'
				)
			),
			'loginAction' => array(
				'controller' => 'admin',
				'action' => 'login'
			)
		),
		'Cookie',
		'Session'
	);
	public function beforeFilter() {
		parent::beforeFilter();
		$locale = Configure::read('Config.language');
		if ($locale && file_exists(APP . 'View' . DS . $locale . DS . $this->viewPath . DS . $this->view . $this->ext)) {
			// e.g. use /app/View/fra/Pages/tos.ctp instead of /app/View/Pages/tos.ctp
			$this->viewPath = $locale . DS . $this->viewPath;
		}
		// set cookie options
		$this->Cookie->key = 'qSI232qs*&sXOw!adre@34SAv!@*(XSL#$%)asGb$@11~_+!@#HKis~#^';
		$this->Cookie->httpOnly = true;
		$this->user = $this->Auth->user();

		// Left sidebar
		$this->loadModel('Category');
		$categories = $this->Category->find('all');
		$this->set(compact('categories'));
		$this->set('controller_title', '');
	}
	public function beforeRender() {
		$this->set('site_title', 'Da Nang Jobs Portal');
		$this->set('current_user',$this->user);
	}
}
