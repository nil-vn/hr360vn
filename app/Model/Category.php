<?php
App::uses('AppModel', 'Model');
class Category extends AppModel {

	// public $useTable = 'categories';
	public $hasMany = array(
		'Document' => array(
			'className' => 'Document',
			'order' => 'Document.created_at DESC'
		)
	);

	public function getCategoriesName($list_categories){
		$category_ids = json_decode($list_categories);
		$cats = array();
		if(!empty($category_ids)){
			try{
				foreach($category_ids as $category_id){
					$category = $this->find('first', array(
						'conditions' => array(
							'Category.id' => $category_id),
						'order' => array('Category.name ASC'),
						'recurse' => -1
					));
					$cats[] = $category['Category'];
				}
			} catch(Exception $e){

			}
		}

		return $cats;
	}
}