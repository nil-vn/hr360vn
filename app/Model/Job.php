<?php
App::uses('AppModel', 'Model');
class Job extends AppModel {

	// public $useTable = 'categories';
	public $belongsTo = array(
		'Company' => array(
			'className' => 'Company')
	);
}