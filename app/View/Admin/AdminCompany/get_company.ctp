<div class="row">
	<div class="col-md-6">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php echo __('General Information');?></h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<div class="box-body">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<td><b><?php echo __('Company name');?></b></td>
							<td><p><?php echo $company['Company']['company_name'];?></p></td>
						</tr>
						<tr>
							<td></td>
							<td><img src="<?php echo $company['Company']['logo_url'];?>" style="height: 60px;" /></td>
						</tr>
						<tr>
							<td><b><?php echo __('Homepage URL');?></b></td>
							<td><?php echo $company['Company']['homepage_url'];?></td>
						</tr>
						<tr>
							<td><b><?php echo __('Introduction');?></b></td>
							<td><?php echo $company['Company']['description'];?></td>
						</tr>
						<tr>
							<td><b><?php echo __('Email');?></b></td>
							<td><?php echo $company['Company']['email'];?></td>
						</tr>
						<tr>
							<td><b><?php echo __('Telephone');?></b></td>
							<td><?php echo $company['Company']['telephone'];?></td>
						</tr>
						<tr>
							<td><b><?php echo __('Location');?></b></td>
							<td><?php echo $company['Company']['location'];?></td>
						</tr>
						<tr>
							<td><b><?php echo __('Career');?></b></td>
							<td><?php echo ($company['Company']['career_id'] == 0) ? 'Restaurant' :
							(($company['Company']['career_id'] == 1) ?  'IT' : 'Business Consulting');?></td>
						</tr>
						<tr>
							<td><b><?php echo __('Status');?></b></td>
							<td><?php echo ($company['Company']['status'] == 0) ? 'Disable' : 'Enable';?></td>
						</tr>
						<tr>
							<td colspan="2">
								<a href="/admin/company/edit/<?php echo $company['Company']['id'];?>" class="btn btn-primary"><?php echo __('Edit');?></a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div><!-- /.box -->
	</div><!--/.col (left) -->
	<div class="col-md-6">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php echo __('More');?></h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<div class="box-body">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<td><b><?php echo __('Created at');?></b></td>
							<td><p><?php echo date('d/m/Y', strtotime($company['Company']['created']));?></p></td>
						</tr>
						<tr>
							<td><b><?php echo __('Created by');?></b></td>
							<td><p>Admin</p></td>
						</tr>
						<tr>
							<td><b><?php echo __('Service type');?></b></td>
							<td><?php echo ($company['Company']['service_type'] == 0) ? 'None' :
							(($company['Company']['career_id'] == 1) ?  'Free' : 'Premium');?></td>
						</tr>
						<tr>
							<td><b><?php echo __('Service available to');?></b></td>
							<td><?php
								$available_to = strtotime($company['Company']['service_available_to']);
								echo date('d/m/Y h:i a', $available_to);
								$now = time();
								if($now > $available_to){
									echo '<span class="badge bg-red" style="float: right;">!</span>';
								} else {
									echo '';
								}
							?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div><!-- /.box -->
	</div><!--/.col (left) -->
</div>