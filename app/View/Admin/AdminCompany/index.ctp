<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php echo __('Company list');?></h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<input type="text" class="form-control input-sm" placeholder="<?php echo __('Search category');?>">
						<span class="glyphicon glyphicon-search form-control-feedback"></span>
					</div>

				</div>
			</div><!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered list-cats">
					<tbody>
						<tr>
							<th style="width: 35px; text-align: center;"><span style="cursor: pointer;"><i class="fa fa-ellipsis-v checkbox-toggle"></i></span></th>
							<td style="width: 50px;"></td>
							<th>Company name</th>
							<th>Service type</th>
							<th>Status</th>
							<th>Available jobs</th>
							<th style="width: 40px">Action</th>
						</tr>
						<?php
						foreach($companies as $key => $company){?>
							<tr class="tr-body">
								<td><input type="checkbox" data-category="<?php echo $company['Company']['id'];?>"/></td>
								<td><img class="" style="height: 20px;" src="<?php echo $company['Company']['logo_url'];?>"/></td>
								<td><a href="/admin/company/<?php echo $company['Company']['id'];?>"><?php echo $company['Company']['company_name'];?></a></td>
								<td><?php
									if($company['Company']['service_type'] == 0){
										echo '<span class="badge bg-light-blue">None</span>';
									}elseif($company['Company']['service_type'] == 1){
										echo '<span class="badge bg-light-blue">Free</span>';
									} else {
										echo '<span class="badge bg-light-blue">Premium</span>';
									}
								?></td>
								<td><?php
									if($company['Company']['status'] == 0){
										echo '<span class="badge bg-red">Disable</span>';
									}else{
										echo '<span class="badge bg-light-blue">Enable</span>';
									}
								?></td>
								<th><p style="font-weight: normal;"><?php echo count($company['Job']);?></p></th>
								<td><a href="/admin/company/edit/<?php echo $company['Company']['id'];?>" class="badge bg-light-blue">Edit</a></td>
							</tr>
						<?php }?>
					</tbody>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				<form action="/admin/category/delete" id="deleteCategoriesForm" style="display: inline;" method="post">
					<button type="submit" class="btn btn-danger"><?php echo __('Delete selected items');?></button>
				</form>
				<ul class="pagination pagination-sm no-margin pull-right">
					<li><a href="#">«</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">»</a></li>
				</ul>
			</div>
		</div><!-- /.box -->
	</div><!--/.col (left) -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php echo __('New Company');?></h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form role="form" action="/admin/company/add" method="post">
				<div class="box-body">
					<div class="form-group">
						<label for="inputCompanyName"><?php echo __('Company name');?></label>
						<input type="text" name="company_name" class="form-control" id="inputCompanyName" placeholder="Company name">
					</div>
					<div class="form-group">
						<label for="inputCompanyHomepage"><?php echo __('Homepage URL');?></label>
						<input type="text" name="homepage_url" class="form-control" id="inputCompanyHomepage" placeholder="Homepage URL">
					</div>
					<div class="form-group">
						<label for="inputCompanyDescription"><?php echo __('Description');?></label>
						<textarea name="description" class="form-control" id="inputCompanyDescription" placeholder="Description" style="height: 300px;"></textarea>
					</div>
					<div class="form-group">
						<label for="inputCompanyLogo"><?php echo __('Logo URL');?></label>
						<input type="text" name="logo_url" class="form-control" id="inputCompanyLogo" placeholder="Logo URL">
					</div>
					<div class="form-group">
						<label for="inputCompanyEmail"><?php echo __('Email');?></label>
						<input type="text" name="email" class="form-control" id="inputCompanyEmail" placeholder="Email">
					</div>
					<div class="form-group">
						<label for="inputCompanyPhone"><?php echo __('Telephone');?></label>
						<input type="text" name="telephone" class="form-control" id="inputCompanyPhone" placeholder="Telephone">
					</div>
					<div class="form-group">
						<label for="inputCompanyLocation"><?php echo __('Location');?></label>
						<input type="text" name="location" class="form-control" id="inputCompanyLocation" placeholder="Location">
					</div>
					<div class="form-group">
						<label>Career</label>
						<select class="form-control" name="career_id">
							<option value="0" selected="selected">Restaurant</option>
							<option value="1">IT</option>
							<option value="2">Business Consulting</option>
						</select>
					</div>
					<div class="form-group">
						<label>Service type</label>
						<select class="form-control" name="service_type">
							<option value="0" selected="selected">None</option>
							<option value="1">Free</option>
							<option value="2">Premium</option>
						</select>
					</div>
					<div class="form-group">
						<label>Status</label>
						<select class="form-control" name="status">
							<option value="0">Disable</option>
							<option value="1" selected="selected">Enable</option>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary"><?php echo __('Create');?></button>
				</div>
			</form>
		</div><!-- /.box -->
	</div><!--/.col (left) -->
</div>
<script type="text/javascript">
	$(function () {
		$('.list-cats input[type="checkbox"]').iCheck({
			checkboxClass: 'icheckbox_flat-blue',
			radioClass: 'iradio_flat-blue'
		});

		//Enable check and uncheck all functionality
		$(".checkbox-toggle").click(function () {
			var clicks = $(this).data('clicks');
			if (clicks) {
				//Uncheck all checkboxes
				$(".list-cats input[type='checkbox']").iCheck("uncheck");
				$(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
			} else {
				//Check all checkboxes
				$(".list-cats input[type='checkbox']").iCheck("check");
				$(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
			}
			$(this).data("clicks", !clicks);
		});

		$('.list-cats input[type="checkbox"]').on('ifChecked',function(){
			var cat_id = $(this).data('category');
			$('#deleteCategoriesForm').append('<input type="hidden" class="category_'+cat_id+'" name="category_id[]" value="'+cat_id+'">');
		});
		$('.list-cats input[type="checkbox"]').on('ifUnchecked',function(){
			var cat_id = $(this).data('category');
			$('#deleteCategoriesForm').find('input.category_'+cat_id).remove();
		});
		// Delete categories
		// $('#deleteCategoriesForm').on('submit', function(){
		// 	return false;
		// });
	});
</script>