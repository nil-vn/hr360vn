<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php echo __('Edit Company');?></h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form role="form" action="" method="post">
				<div class="box-body">
					<div class="form-group">
						<label for="inputCompanyName"><?php echo __('Company name');?></label>
						<input type="text" name="company_name" class="form-control" id="inputCompanyName" placeholder="Company name" value="<?php echo $company['Company']['company_name'];?>">
					</div>
					<div class="form-group">
						<label for="inputCompanyHomepage"><?php echo __('Homepage URL');?></label>
						<input type="text" name="homepage_url" class="form-control" id="inputCompanyHomepage" placeholder="Homepage URL" value="<?php echo $company['Company']['homepage_url'];?>">
					</div>
					<div class="form-group">
						<label for="inputCompanyDescription"><?php echo __('Description');?></label>
						<textarea name="description" class="form-control" id="inputCompanyDescription" placeholder="Description" style="height: 300px;"><?php echo $company['Company']['description'];?></textarea>
					</div>
					<div class="form-group">
						<label for="inputCompanyLogo"><?php echo __('Logo URL');?></label>
						<input type="text" name="logo_url" class="form-control" id="inputCompanyLogo" placeholder="Logo URL" value="<?php echo $company['Company']['logo_url'];?>">
					</div>
					<div class="form-group">
						<label for="inputCompanyEmail"><?php echo __('Email');?></label>
						<input type="text" name="email" class="form-control" id="inputCompanyEmail" placeholder="Email" value="<?php echo $company['Company']['email'];?>">
					</div>
					<div class="form-group">
						<label for="inputCompanyPhone"><?php echo __('Telephone');?></label>
						<input type="text" name="telephone" class="form-control" id="inputCompanyPhone" placeholder="Telephone" value="<?php echo $company['Company']['telephone'];?>">
					</div>
					<div class="form-group">
						<label for="inputCompanyLocation"><?php echo __('Location');?></label>
						<input type="text" name="location" class="form-control" id="inputCompanyLocation" placeholder="Location" value="<?php echo $company['Company']['location'];?>">
					</div>
					<div class="form-group">
						<label>Career</label>
						<select class="form-control" name="career_id">
							<option value="0">Restaurant</option>
							<option value="1">IT</option>
							<option value="2">Business Consulting</option>
						</select>
					</div>
					<div class="form-group">
						<label>Service type</label>
						<select class="form-control" name="service_type">
							<option value="0"<?php if($company['Company']['service_type'] == 0){ echo 'selected="selected"';}?>>None</option>
							<option value="1"<?php if($company['Company']['service_type'] == 1){ echo 'selected="selected"';}?>>Free</option>
							<option value="2"<?php if($company['Company']['service_type'] == 2){ echo 'selected="selected"';}?>>Premium</option>
						</select>
					</div>
					<div class="form-group">
						<label>Status</label>
						<select class="form-control" name="status">
							<option value="0"<?php if($company['Company']['status'] == 0){ echo 'selected="selected"';}?>>Disable</option>
							<option value="1"<?php if($company['Company']['status'] == 1){ echo 'selected="selected"';}?>>Enable</option>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary"><?php echo __('Save');?></button>
				</div>
			</form>
		</div><!-- /.box -->
	</div><!--/.col (left) -->
</div>
<script type="text/javascript">
	$(function () {
		$('.list-cats input[type="checkbox"]').iCheck({
			checkboxClass: 'icheckbox_flat-blue',
			radioClass: 'iradio_flat-blue'
		});

		//Enable check and uncheck all functionality
		$(".checkbox-toggle").click(function () {
			var clicks = $(this).data('clicks');
			if (clicks) {
				//Uncheck all checkboxes
				$(".list-cats input[type='checkbox']").iCheck("uncheck");
				$(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
			} else {
				//Check all checkboxes
				$(".list-cats input[type='checkbox']").iCheck("check");
				$(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
			}
			$(this).data("clicks", !clicks);
		});

		$('.list-cats input[type="checkbox"]').on('ifChecked',function(){
			var cat_id = $(this).data('category');
			$('#deleteCategoriesForm').append('<input type="hidden" class="category_'+cat_id+'" name="category_id[]" value="'+cat_id+'">');
		});
		$('.list-cats input[type="checkbox"]').on('ifUnchecked',function(){
			var cat_id = $(this).data('category');
			$('#deleteCategoriesForm').find('input.category_'+cat_id).remove();
		});
		// Delete categories
		// $('#deleteCategoriesForm').on('submit', function(){
		// 	return false;
		// });
	});
</script>