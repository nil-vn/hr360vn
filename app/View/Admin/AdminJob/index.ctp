<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php echo __('Available Jobs');?></h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<input type="text" class="form-control input-sm" placeholder="<?php echo __('Search jobs');?>">
						<span class="glyphicon glyphicon-search form-control-feedback"></span>
					</div>
				</div>
			</div><!-- /.box-header -->
			<div class="box-body">
				<span class="add-job btn btn-primary"><a href="/admin/jobs/add"><?php echo __('Add new job +');?></a></span>
				<table class="table table-bordered list-cats">
					<tbody>
						<tr>
							<th style="width: 35px; text-align: center;"><span style="cursor: pointer;"><i class="fa fa-ellipsis-v checkbox-toggle"></i></span></th>
							<th>Job title</th>
							<th>Status</th>
							<th>Categories</th>
							<th>Company</th>
							<th>Created at</th>
							<th>Available to</th>
							<th style="width: 40px">Action</th>
						</tr>
						<?php
						foreach($jobs as $key => $job){?>
							<tr class="tr-body">
								<td><input type="checkbox" data-category="<?php echo $job['Job']['id'];?>"/></td>
								<td><?php echo $job['Job']['job_title'];?>
								</td>
								<td><?php
									if($job['Job']['is_hot']){
										echo '<span class="category-tags label label-info">Hot</span>';
									}
									if($job['Job']['is_urgent']){
										echo '<span class="category-tags label label-info">Urgent</span>';
									}
									if($job['Job']['is_new']){
										echo '<span class="category-tags label label-info">New</span>';
									}
								?></td>
								<td><?php
								foreach($job['Categories'] as $parent_category){
									echo '<span class="category-tags label label-info">'.$parent_category['name'].'</span>';
								}
								?></td>
								<td style="text-align: center;"><img src="<?php echo $job['Company']['logo_url'];?>" style="height: 20px;"></td>
								<td><?php echo $job['Job']['created'];?></td>
								<td><?php echo $job['Job']['available_to'];?></td>
								<td><a href="/admin/jobs/edit/<?php echo $job['Job']['id'];?>" class="badge bg-light-blue">Edit</a></td>
							</tr>
						<?php }?>
					</tbody>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				<form action="/admin/jobs/delete" id="deleteCategoriesForm" style="display: inline; float: left;" method="post">
					<button type="submit" class="btn btn-danger"><?php echo __('Delete selected items');?></button>
				</form>
				<div id="jobinfomation" style="margin: 20px; display: inline; line-height: 30px;">Available 9999 jobs in 99 categories</div>
				<ul class="pagination pagination-sm no-margin pull-right">
					<li><a href="#">«</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">»</a></li>
				</ul>
			</div>
		</div><!-- /.box -->
	</div><!--/.col (left) -->
</div>
<script type="text/javascript">
	$(function () {
		$('.list-cats input[type="checkbox"]').iCheck({
			checkboxClass: 'icheckbox_flat-blue',
			radioClass: 'iradio_flat-blue'
		});

		//Enable check and uncheck all functionality
		$(".checkbox-toggle").click(function () {
			var clicks = $(this).data('clicks');
			if (clicks) {
				//Uncheck all checkboxes
				$(".list-cats input[type='checkbox']").iCheck("uncheck");
				$(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
			} else {
				//Check all checkboxes
				$(".list-cats input[type='checkbox']").iCheck("check");
				$(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
			}
			$(this).data("clicks", !clicks);
		});

		$('.list-cats input[type="checkbox"]').on('ifChecked',function(){
			var cat_id = $(this).data('category');
			$('#deleteCategoriesForm').append('<input type="hidden" class="category_'+cat_id+'" name="category_id[]" value="'+cat_id+'">');
		});
		$('.list-cats input[type="checkbox"]').on('ifUnchecked',function(){
			var cat_id = $(this).data('category');
			$('#deleteCategoriesForm').find('input.category_'+cat_id).remove();
		});
		// Delete categories
		// $('#deleteCategoriesForm').on('submit', function(){
		// 	return false;
		// });
	});
</script>