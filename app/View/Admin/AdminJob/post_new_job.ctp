<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php echo __('New Job');?></h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form role="form" action="/admin/jobs/add" method="post">
				<div class="box-body">
					<div class="form-group">
						<label for="inputJobTitle"><?php echo __('Job title');?></label>
						<input type="text" name="job_title" class="form-control" id="inputJobTitle" placeholder="Job title">
					</div>
					<div class="form-group">
						<label for="inputJobDescription"><?php echo __('Description');?></label>
						<textarea name="job_description" class="form-control" id="inputJobDescription" placeholder="Description"></textarea>
					</div>
					<div class="form-group">
						<label>Select company</label>
						<select class="form-control" name="company_id">
							<option value="0">None</option>
							<?php foreach ($companies as $key => $company) {?>
								<option value="<?php echo $company['Company']['id'];?>"><?php echo $company['Company']['company_name'];?></option>
							<?php }?>
						</select>
					</div>
					<div class="form-group">
						<label for="inputJobLocation"><?php echo __('Work location');?></label>
						<input type="text" name="location" class="form-control" id="inputJobLocation" placeholder="Work location">
					</div>
					<div class="form-group">
						<label for="inputJobSalary"><?php echo __('Salary');?></label>
						<input type="text" name="salary" class="form-control" id="inputJobSalary" placeholder="Salary">
					</div>
					<div class="form-group">
						<label for="inputJobBenefit"><?php echo __('Benefit');?></label>
						<textarea name="benefit" class="form-control" id="inputJobBenefit" placeholder="Benefit"></textarea>
					</div>
					<div class="form-group">
						<label for="inputJobRequired"><?php echo __('Requires');?></label>
						<textarea name="required" class="form-control" id="inputJobRequired" placeholder="Requires"></textarea>
					</div>
					<div class="form-group">
						<label>Available to:</label>
						<div class='input-group date specialDate' style="width: 250px;">
                                <input type='text' name="endDate" class="form-control" value=""/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        </div>
					</div>
					<div class="form-group">
						<label>Select categories</label>
						<select data-placeholder="Type 'category name' to view" style="width:350px;" multiple class="chosen-select-no-results" tabindex="11" class="form-control" name="category[]">
				            <?php foreach ($categories as $key => $category) {
								?>
								<option value="<?php echo $category['Category']['id'];?>"><?php echo $category['Category']['name'];?></option>
							<?php }?>
				          </select>
					</div>
					<div class="form-group">
						<label>Is hot job?</label>
						<select style="width:350px;" class="form-control" name="is_hot">
							<option value="0">No</option>
							<option value="1">Yes</option>
						</select>
					</div>
					<div class="form-group">
						<label>Is urgent job?</label>
						<select style="width:350px;" class="form-control" name="is_urgent">
							<option value="0">No</option>
							<option value="1">Yes</option>
						</select>
					</div>
					<div class="form-group">
						<label>Is new job?</label>
						<select style="width:350px;" class="form-control" name="is_new">
							<option value="0">No</option>
							<option value="1">Yes</option>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary"><?php echo __('Save');?></button>
				</div>
			</form>
		</div><!-- /.box -->
	</div><!--/.col (left) -->
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$('.chosen-select-no-results').chosen({no_results_text:'Oops, nothing found!'});
		$('.specialDate').datetimepicker();
	});
</script>