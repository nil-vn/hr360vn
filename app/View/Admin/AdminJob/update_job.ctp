<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php echo __('Edit Job');?></h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form role="form" action="" method="post">
				<div class="box-body">
					<div class="form-group">
						<label for="inputJobTitle"><?php echo __('Job title');?></label>
						<input type="text" name="job_title" class="form-control" id="inputJobTitle" placeholder="Job title" value="<?php echo $job['Job']['job_title'];?>">
					</div>
					<div class="form-group">
						<label for="inputJobDescription"><?php echo __('Description');?></label>
						<textarea name="job_description" class="form-control" id="inputJobDescription" placeholder="Description" style="height: auto;"><?php echo $job['Job']['job_description'];?></textarea>
					</div>
					<div class="form-group">
						<label>Select company</label>
						<select class="form-control" name="company_id">
							<option value="0">None</option>
							<?php foreach ($companies as $key => $company) {
								if($job['Job']['company_id'] == $company['Company']['id']){
									$selected = "selected='selected'";
								} else {
									$selected = "";
								}?>
								<option value="<?php echo $company['Company']['id'];?>"<?php echo $selected;?>><?php echo $company['Company']['company_name'];?></option>
							<?php }?>
						</select>
					</div>
					<div class="form-group">
						<label for="inputJobLocation"><?php echo __('Work location');?></label>
						<input type="text" name="location" class="form-control" id="inputJobLocation" placeholder="Work location" value="<?php echo $job['Job']['location'];?>">
					</div>
					<div class="form-group">
						<label for="inputJobSalary"><?php echo __('Salary');?></label>
						<div class="input-group">
							<input type="text" name="salary" class="form-control" id="inputJobSalary" placeholder="Salary" value="<?php echo $job['Job']['salary'];?>">
							<span class="input-group-addon">Triệu đồng</span>
						</div>
					</div>
					<div class="form-group">
						<label for="inputJobBenefit"><?php echo __('Benefit');?></label>
						<textarea name="benefit" class="form-control" id="inputJobBenefit" placeholder="Benefit" style="height: auto;"><?php echo $job['Job']['benefit'];?></textarea>
					</div>
					<div class="form-group">
						<label for="inputJobRequired"><?php echo __('Requires');?></label>
						<textarea name="required" class="form-control" id="inputJobRequired" placeholder="Requires" style="height: auto;"><?php echo $job['Job']['required'];?></textarea>
					</div>
					<div class="form-group">
						<label>Available to:</label>
						<div class='input-group date specialDate' style="width: 250px;">
                                <input type='text' name="endDate" class="form-control" value="<?php echo date('m/d/Y h:i:s', strtotime($job['Job']['available_to']));?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        </div>
					</div>
					<div class="form-group">
						<label>Select categories</label>
						<select data-placeholder="Type 'category name' to view" style="width:350px;" multiple class="chosen-select-no-results" tabindex="11" class="form-control" name="category[]">
							<?php foreach ($cats as $key => $category) {
								if(in_array($category['Category']['id'], $parent_categories)){
									$selected = " selected='selected'";
								} else {
									$selected = "";
								}
								?>
								<option value="<?php echo $category['Category']['id'];?>"<?php echo $selected;?>><?php echo $category['Category']['name'];?></option>
							<?php }?>
				          </select>
					</div>
					<div class="form-group">
						<label>Is hot job?</label>
						<select style="width:350px;" class="form-control" name="is_hot">
							<?php if($job['Job']['is_hot']){ ?>
							<option value="0">No</option>
							<option value="1" selected="selected">Yes</option>
							<?php } else { ?>
							<option value="0" selected="selected">No</option>
							<option value="1">Yes</option>
							<?php }?>
						</select>
					</div>
					<div class="form-group">
						<label>Is urgent job?</label>
						<select style="width:350px;" class="form-control" name="is_urgent">
							<?php if($job['Job']['is_urgent']){ ?>
							<option value="0">No</option>
							<option value="1" selected="selected">Yes</option>
							<?php } else { ?>
							<option value="0" selected="selected">No</option>
							<option value="1">Yes</option>
							<?php }?>
						</select>
					</div>
					<div class="form-group">
						<label>Is new job?</label>
						<select style="width:350px;" class="form-control" name="is_new">
							<?php if($job['Job']['is_new']){ ?>
							<option value="0">No</option>
							<option value="1" selected="selected">Yes</option>
							<?php } else { ?>
							<option value="0" selected="selected">No</option>
							<option value="1">Yes</option>
							<?php }?>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary"><?php echo __('Save');?></button>
				</div>
			</form>
		</div><!-- /.box -->
	</div><!--/.col (left) -->
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$('.chosen-select-no-results').chosen({no_results_text:'Oops, nothing found!'});
		$('.specialDate').datetimepicker();
	});
</script>