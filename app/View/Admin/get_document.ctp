<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo __('Document');?></h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<input type="text" class="form-control input-sm" placeholder="<?php echo __('Search document');?>">
						<span class="glyphicon glyphicon-search form-control-feedback"></span>
					</div>

				</div>
			</div><!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered list-cats">
					<tbody>
						<tr>
							<th style="width: 35px;"><button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button></th>
							<th>Title</th>
							<th>Type</th>
							<th>Category</th>
							<th>Created date</th>
							<th>Status</th>
							<th style="width: 40px">Action</th>
						</tr>
						<?php
						if(count($category['Document']) > 0){
							foreach($category['Document'] as $key => $document){?>
								<tr class="tr-body">
									<td><input type="checkbox" data-category="<?php echo $category['Category']['id'];?>"/></td>
									<td><?php echo $category['Category']['name'];?></td>
									<td><a href="/admin/setting/category/edit/<?php echo $category['Category']['id'];?>" class="badge bg-light-blue">Edit</a></td>
								</tr>
							<?php }
						} else {?>
							<tr class="tr-body">
									<td colspan="7">Nothing to show</td>
								</tr>
						<?php }?>
					</tbody>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				<form action="/admin/setting/category/delete" id="deleteCategoriesForm" style="display: inline;" method="post">
					<button type="submit" class="btn btn-danger"><?php echo __('Delete selected items');?></button>
				</form>
				<ul class="pagination pagination-sm no-margin pull-right">
					<li><a href="#">«</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">»</a></li>
				</ul>
			</div>
		</div><!-- /.box -->
	</div><!--/.col (left) -->
	<div class="col-md-8">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Upload</h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form role="form" action="/admin/document/upload" method="post">
				<div class="box-body">
					<div class="form-group">
						<label for="inputTitle">Title</label>
						<input type="text" class="form-control" id="inputTitle" placeholder="Document title" name="title">
					</div>
					<div class="form-group">
						<label>Select category</label>
						<select class="form-control" name="parent">
							<option vale="0" selected="selected">None</option>
							<?php foreach ($categories as $key => $category) {
								?>
								<option value="<?php echo $category['Category']['id'];?>"><?php echo $category['Category']['name'];?></option>
							<?php }?>
						</select>
					</div>
					<div class="form-group">
						<label for="exampleInputFile">File input</label>
						<input type="file" id="exampleInputFile">
						<p class="help-block">Maximum size: 10MB</p>
					</div>
				</div><!-- /.box-body -->

				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div><!-- /.box -->
	</div>
	<div class="col-md-4">
		<!-- general form elements -->
		<div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Browser Usage</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-8">
                      <div class="chart-responsive">
                        <canvas id="pieChart" height="160" width="195" style="width: 195px; height: 160px;"></canvas>
                      </div><!-- ./chart-responsive -->
                    </div><!-- /.col -->
                    <div class="col-md-4">
                      <ul class="chart-legend clearfix">
                        <li><i class="fa fa-circle-o text-red"></i> Chrome</li>
                        <li><i class="fa fa-circle-o text-green"></i> IE</li>
                        <li><i class="fa fa-circle-o text-yellow"></i> FireFox</li>
                        <li><i class="fa fa-circle-o text-aqua"></i> Safari</li>
                        <li><i class="fa fa-circle-o text-light-blue"></i> Opera</li>
                        <li><i class="fa fa-circle-o text-gray"></i> Navigator</li>
                      </ul>
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
                <div class="box-footer no-padding">
                  <ul class="nav nav-pills nav-stacked">
                    <li><a href="#">United States of America <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                    <li><a href="#">India <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a></li>
                    <li><a href="#">China <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li>
                  </ul>
                </div><!-- /.footer -->
              </div>
	</div><!--/.col (left) -->
</div>
<script type="text/javascript">
	$(function () {
		$('.list-cats input[type="checkbox"]').iCheck({
			checkboxClass: 'icheckbox_flat-blue',
			radioClass: 'iradio_flat-blue'
		});

		//Enable check and uncheck all functionality
		$(".checkbox-toggle").click(function () {
			var clicks = $(this).data('clicks');
			if (clicks) {
				//Uncheck all checkboxes
				$(".list-cats input[type='checkbox']").iCheck("uncheck");
				$(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
			} else {
				//Check all checkboxes
				$(".list-cats input[type='checkbox']").iCheck("check");
				$(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
			}
			$(this).data("clicks", !clicks);
		});

		$('.list-cats input[type="checkbox"]').on('ifChecked',function(){
			var cat_id = $(this).data('category');
			$('#deleteCategoriesForm').append('<input type="hidden" class="category_'+cat_id+'" name="category_id[]" value="'+cat_id+'">');
		});
		$('.list-cats input[type="checkbox"]').on('ifUnchecked',function(){
			var cat_id = $(this).data('category');
			$('#deleteCategoriesForm').find('input.category_'+cat_id).remove();
		});
		// Delete categories
		// $('#deleteCategoriesForm').on('submit', function(){
		// 	return false;
		// });
	});
</script>