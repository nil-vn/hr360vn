<div class="row">
	<div class="col-md-6">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php echo __('Edit Category');?></h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form role="form" action="" method="post">
				<div class="box-body">
					<div class="form-group">
						<label for="inputCategoryName"><?php echo __('Category name');?></label>
						<input type="text" name="name" class="form-control" id="inputCategoryName" placeholder="Category name" value="<?php echo $category['Category']['name'];?>">
					</div>
					<div class="form-group">
						<label for="inputCategorySlug"><?php echo __('Slug');?></label>
						<input type="text" name="slug" class="form-control" id="inputCategorySlug" placeholder="Slug" value="<?php echo $category['Category']['slug'];?>">
					</div>
					<div class="form-group">
						<label>Select parent</label>
						<select class="form-control" name="parent">
							<?php if($category['Category']['parent'] == '0'){
									$selected = "selected='selected'";
								} else {
									$selected = "";
							}?>
							<option vale="0"<?php echo $selected;?>>None</option>
							<?php foreach ($categories as $key => $cat) {
								if($category['Category']['parent'] == $cat['Category']['id']){
									$selected = "selected='selected'";
								} else {
									$selected = "";
								}
								?>
								<option value="<?php echo $category['Category']['id'];?>"<?php echo $selected;?>><?php echo $cat['Category']['name'];?></option>
							<?php }?>
						</select>
						</div>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary"><?php echo __('Save');?></button>
				</div>
			</form>
		</div><!-- /.box -->
	</div><!--/.col (left) -->
	<div class="col-md-6">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php echo __('Available categories');?></h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<input type="text" class="form-control input-sm" placeholder="<?php echo __('Search category');?>">
						<span class="glyphicon glyphicon-search form-control-feedback"></span>
					</div>

				</div>
			</div><!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered list-cats">
					<tbody>
						<tr>
							<th style="width: 35px;"><button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button></th>
							<th>Category name</th>
							<th style="width: 40px">Action</th>
						</tr>
						<?php
						foreach($cats as $key => $category){?>
							<tr>
								<td><input type="checkbox" data-category="<?php echo $category['Category']['id'];?>"/></td>
								<td><?php echo $category['Category']['name'];?></td>
								<td><a href="/admin/setting/category/edit/<?php echo $category['Category']['id'];?>" class="badge bg-light-blue">Edit</a></td>
							</tr>
						<?php }?>
					</tbody>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				<form action="/admin/setting/category/delete" id="deleteCategoriesForm" style="display: inline;" method="post">
					<button type="submit" class="btn btn-danger"><?php echo __('Delete selected items');?></button>
				</form>
				<ul class="pagination pagination-sm no-margin pull-right">
					<li><a href="#">«</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">»</a></li>
				</ul>
			</div>
		</div><!-- /.box -->
	</div><!--/.col (left) -->
</div>
<script type="text/javascript">
	$(function () {
		$('.list-cats input[type="checkbox"]').iCheck({
			checkboxClass: 'icheckbox_flat-blue',
			radioClass: 'iradio_flat-blue'
		});

		//Enable check and uncheck all functionality
		$(".checkbox-toggle").click(function () {
			var clicks = $(this).data('clicks');
			if (clicks) {
				//Uncheck all checkboxes
				$(".list-cats input[type='checkbox']").iCheck("uncheck");
				$(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
			} else {
				//Check all checkboxes
				$(".list-cats input[type='checkbox']").iCheck("check");
				$(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
			}
			$(this).data("clicks", !clicks);
		});

		$('.list-cats input[type="checkbox"]').on('ifChecked',function(){
			var cat_id = $(this).data('category');
			$('#deleteCategoriesForm').append('<input type="hidden" class="category_'+cat_id+'" name="category_id[]" value="'+cat_id+'">');
		});
		$('.list-cats input[type="checkbox"]').on('ifUnchecked',function(){
			var cat_id = $(this).data('category');
			$('#deleteCategoriesForm').find('input.category_'+cat_id).remove();
		});
	});
</script>