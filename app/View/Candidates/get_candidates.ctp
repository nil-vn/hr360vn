<div class="container">
	<div class="row">
		<div class="col-sm-9">
			<h2 class="page-header"><strong>215</strong> ứng viên đang tìm kiếm công việc</h2>
            <div class="candidates-list">
            	<div class="candidates-list-item">
                    <div class="candidates-list-item-heading">
                		<div class="candidates-list-item-image">
                			<a href="#">
                				<img src="/assets/img/tmp/resume.jpg" alt="Elliot Sarah Scott">
                			</a>
                            <a href="#" class="candidates-list-item-image-label">
                                <img src="/assets/img/tmp/instagram.png" alt="">
                            </a>
                        </div><!-- /.candidates-list-item-image -->
                        <div class="candidates-list-item-title">
                            <h2><a href="/candidate/elliot-sarah-scott">Elliot Sarah Scott</a></h2>
                            <h3>Data Analytist</h3>
                        </div><!-- /.candidates-list-item-title -->
                    </div><!-- /.candidates-list-item-heading -->

                    <div class="candidates-list-item-location">
                        <i class="fa fa-map-marker"></i> San Francisco, California
                    </div><!-- /.candidates-list-item-location -->

                    <div class="candidates-list-item-profile">
                        <span class="candidates-list-item-profile-count">92% completed</span>
                    	<span class="candidates-list-item-profile-bar"><span style="width: 92%"></span></span>
                    </div><!-- /.candidates-list-item-rating -->
                </div><!-- /.candidates-list-item -->

            	<div class="candidates-list-item">
                    <div class="candidates-list-item-heading">
                		<div class="candidates-list-item-image">
                			<a href="#">
                				<img src="/assets/img/tmp/resume-1.jpg" alt="Peter Ruck">
                			</a>


                		</div><!-- /.candidates-list-item-image -->

                        <div class="candidates-list-item-title">
                            <h2><a href="/candidate/elliot-sarah-scott">Peter Ruck</a></h2>
                            <h3>Java Developer</h3>
                        </div><!-- /.candidates-list-item-title -->
                    </div><!-- /.candidates-list-item-heading -->

                    <div class="candidates-list-item-location">
                        <i class="fa fa-map-marker"></i> New York City, New York
                    </div><!-- /.candidates-list-item-location -->

                    <div class="candidates-list-item-profile">
                        <span class="candidates-list-item-profile-count">68% completed</span>
                    	<span class="candidates-list-item-profile-bar"><span style="width: 68%"></span></span>
                    </div><!-- /.candidates-list-item-rating -->
            	</div><!-- /.candidates-list-item -->

            	<div class="candidates-list-item">
                    <div class="candidates-list-item-heading">
                		<div class="candidates-list-item-image">
                			<a href="#">
                				<img src="/assets/img/tmp/resume-2.jpg" alt="Linda Young">
                			</a>


                                <a href="#" class="candidates-list-item-image-label">
                                    <img src="/assets/img/tmp/dropbox.png" alt="">
                                </a>

                		</div><!-- /.candidates-list-item-image -->

                        <div class="candidates-list-item-title">
                            <h2><a href="/candidate/elliot-sarah-scott">Linda Young</a></h2>
                            <h3>PR Manager</h3>
                        </div><!-- /.candidates-list-item-title -->
                    </div><!-- /.candidates-list-item-heading -->

                    <div class="candidates-list-item-location">
                        <i class="fa fa-map-marker"></i> Chicago, Michigan
                    </div><!-- /.candidates-list-item-location -->

                    <div class="candidates-list-item-profile">
                        <span class="candidates-list-item-profile-count">78% completed</span>
                    	<span class="candidates-list-item-profile-bar"><span style="width: 78%"></span></span>
                    </div><!-- /.candidates-list-item-rating -->
            	</div><!-- /.candidates-list-item -->

            	<div class="candidates-list-item">
                    <div class="candidates-list-item-heading">
                		<div class="candidates-list-item-image">
                			<a href="#">
                				<img src="/assets/img/tmp/resume-3.jpg" alt="Britney Doe">
                			</a>


                		</div><!-- /.candidates-list-item-image -->

                        <div class="candidates-list-item-title">
                            <h2><a href="/candidate/elliot-sarah-scott">Britney Doe</a></h2>
                            <h3>Data Mining</h3>
                        </div><!-- /.candidates-list-item-title -->
                    </div><!-- /.candidates-list-item-heading -->

                    <div class="candidates-list-item-location">
                        <i class="fa fa-map-marker"></i> Philadelphia, Pennsylvania
                    </div><!-- /.candidates-list-item-location -->

                    <div class="candidates-list-item-profile">
                        <span class="candidates-list-item-profile-count">82% completed</span>
                    	<span class="candidates-list-item-profile-bar"><span style="width: 82%"></span></span>
                    </div><!-- /.candidates-list-item-rating -->
            	</div><!-- /.candidates-list-item -->

            	<div class="candidates-list-item">
                    <div class="candidates-list-item-heading">
                		<div class="candidates-list-item-image">
                			<a href="#">
                				<img src="/assets/img/tmp/resume-4.jpg" alt="Andrew Man">
                			</a>


                                <a href="#" class="candidates-list-item-image-label">
                                    <img src="/assets/img/tmp/airbnb.png" alt="">
                                </a>

                		</div><!-- /.candidates-list-item-image -->

                        <div class="candidates-list-item-title">
                            <h2><a href="/candidate/elliot-sarah-scott">Andrew Man</a></h2>
                            <h3>Python Developer</h3>
                        </div><!-- /.candidates-list-item-title -->
                    </div><!-- /.candidates-list-item-heading -->

                    <div class="candidates-list-item-location">
                        <i class="fa fa-map-marker"></i> Denver, Colorado
                    </div><!-- /.candidates-list-item-location -->

                    <div class="candidates-list-item-profile">
                        <span class="candidates-list-item-profile-count">70% completed</span>
                    	<span class="candidates-list-item-profile-bar"><span style="width: 70%"></span></span>
                    </div><!-- /.candidates-list-item-rating -->
            	</div><!-- /.candidates-list-item -->

            	<div class="candidates-list-item">
                    <div class="candidates-list-item-heading">
                		<div class="candidates-list-item-image">
                			<a href="#">
                				<img src="/assets/img/tmp/resume.jpg" alt="Elliot Sarah Scott">
                			</a>


                                <a href="#" class="candidates-list-item-image-label">
                                    <img src="/assets/img/tmp/instagram.png" alt="">
                                </a>

                		</div><!-- /.candidates-list-item-image -->

                        <div class="candidates-list-item-title">
                            <h2><a href="/candidate/elliot-sarah-scott">Elliot Sarah Scott</a></h2>
                            <h3>Data Analytist</h3>
                        </div><!-- /.candidates-list-item-title -->
                    </div><!-- /.candidates-list-item-heading -->

                    <div class="candidates-list-item-location">
                        <i class="fa fa-map-marker"></i> San Francisco, California
                    </div><!-- /.candidates-list-item-location -->

                    <div class="candidates-list-item-profile">
                        <span class="candidates-list-item-profile-count">92% completed</span>
                    	<span class="candidates-list-item-profile-bar"><span style="width: 92%"></span></span>
                    </div><!-- /.candidates-list-item-rating -->
            	</div><!-- /.candidates-list-item -->

            	<div class="candidates-list-item">
                    <div class="candidates-list-item-heading">
                		<div class="candidates-list-item-image">
                			<a href="#">
                				<img src="/assets/img/tmp/resume-1.jpg" alt="Peter Ruck">
                			</a>


                		</div><!-- /.candidates-list-item-image -->

                        <div class="candidates-list-item-title">
                            <h2><a href="/candidate/elliot-sarah-scott">Peter Ruck</a></h2>
                            <h3>Java Developer</h3>
                        </div><!-- /.candidates-list-item-title -->
                    </div><!-- /.candidates-list-item-heading -->

                    <div class="candidates-list-item-location">
                        <i class="fa fa-map-marker"></i> New York City, New York
                    </div><!-- /.candidates-list-item-location -->

                    <div class="candidates-list-item-profile">
                        <span class="candidates-list-item-profile-count">68% completed</span>
                    	<span class="candidates-list-item-profile-bar"><span style="width: 68%"></span></span>
                    </div><!-- /.candidates-list-item-rating -->
            	</div><!-- /.candidates-list-item -->

            	<div class="candidates-list-item">
                    <div class="candidates-list-item-heading">
                		<div class="candidates-list-item-image">
                			<a href="#">
                				<img src="/assets/img/tmp/resume-2.jpg" alt="Linda Young">
                			</a>


                                <a href="#" class="candidates-list-item-image-label">
                                    <img src="/assets/img/tmp/dropbox.png" alt="">
                                </a>

                		</div><!-- /.candidates-list-item-image -->

                        <div class="candidates-list-item-title">
                            <h2><a href="/candidate/elliot-sarah-scott">Linda Young</a></h2>
                            <h3>PR Manager</h3>
                        </div><!-- /.candidates-list-item-title -->
                    </div><!-- /.candidates-list-item-heading -->

                    <div class="candidates-list-item-location">
                        <i class="fa fa-map-marker"></i> Chicago, Michigan
                    </div><!-- /.candidates-list-item-location -->

                    <div class="candidates-list-item-profile">
                        <span class="candidates-list-item-profile-count">78% completed</span>
                    	<span class="candidates-list-item-profile-bar"><span style="width: 78%"></span></span>
                    </div><!-- /.candidates-list-item-rating -->
            	</div><!-- /.candidates-list-item -->

            	<div class="candidates-list-item">
                    <div class="candidates-list-item-heading">
                		<div class="candidates-list-item-image">
                			<a href="#">
                				<img src="/assets/img/tmp/resume-3.jpg" alt="Britney Doe">
                			</a>


                		</div><!-- /.candidates-list-item-image -->

                        <div class="candidates-list-item-title">
                            <h2><a href="/candidate/elliot-sarah-scott">Britney Doe</a></h2>
                            <h3>Data Mining</h3>
                        </div><!-- /.candidates-list-item-title -->
                    </div><!-- /.candidates-list-item-heading -->

                    <div class="candidates-list-item-location">
                        <i class="fa fa-map-marker"></i> Philadelphia, Pennsylvania
                    </div><!-- /.candidates-list-item-location -->

                    <div class="candidates-list-item-profile">
                        <span class="candidates-list-item-profile-count">82% completed</span>
                    	<span class="candidates-list-item-profile-bar"><span style="width: 82%"></span></span>
                    </div><!-- /.candidates-list-item-rating -->
            	</div><!-- /.candidates-list-item -->

            	<div class="candidates-list-item">
                    <div class="candidates-list-item-heading">
                		<div class="candidates-list-item-image">
                			<a href="#">
                				<img src="/assets/img/tmp/resume-4.jpg" alt="Andrew Man">
                			</a>


                                <a href="#" class="candidates-list-item-image-label">
                                    <img src="/assets/img/tmp/airbnb.png" alt="">
                                </a>

                		</div><!-- /.candidates-list-item-image -->

                        <div class="candidates-list-item-title">
                            <h2><a href="/candidate/elliot-sarah-scott">Andrew Man</a></h2>
                            <h3>Python Developer</h3>
                        </div><!-- /.candidates-list-item-title -->
                    </div><!-- /.candidates-list-item-heading -->

                    <div class="candidates-list-item-location">
                        <i class="fa fa-map-marker"></i> Denver, Colorado
                    </div><!-- /.candidates-list-item-location -->

                    <div class="candidates-list-item-profile">
                        <span class="candidates-list-item-profile-count">70% completed</span>
                    	<span class="candidates-list-item-profile-bar"><span style="width: 70%"></span></span>
                    </div><!-- /.candidates-list-item-rating -->
            	</div><!-- /.candidates-list-item -->

            </div><!-- /.candidates-list -->

    		<div class="center">
            	<ul class="pagination">
            		<li>
            			<a href="#">
            				<i class="fa fa-chevron-left"></i>
            			</a>
            		</li>

            		<li><a href="#">1</a></li>
            		<li><a href="#">2</a></li>
            		<li class="active"><a href="#">3</a></li>
            		<li><a href="#">4</a></li>
            		<li><a href="#">5</a></li>
            		<li>
            			<a href="#">
            				<i class="fa fa-chevron-right"></i>
            			</a>
            		</li>
            	</ul>
            </div><!-- /.center -->
        </div><!-- /.col-* -->

	    <div class="col-sm-3">
            <div class="filter-stacked">
                <form method="post" action="?">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Từ khóa">
                    </div>

                    <h3>Lương <a href="#"><i class="fa fa-close"></i></a></h3>

                    <div class="split-forms">
                        <div class="form-group">
                            <input type="number" class="form-control" placeholder="Min.">
                        </div>

                        <div class="form-group">
                            <input type="number" class="form-control" placeholder="Max.">
                        </div>
                    </div><!-- /.split-forms -->

                    <h3>Loại công việc <a href="#"><i class="fa fa-close"></i></a></h3>

                    <div class="checkbox">
                        <label><input type="checkbox"> Toàn thời gian</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="checkbox"> Bán thời gian</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="checkbox"> Các dự án 1 lần</label>
                    </div><!-- /.checkbox -->

                    <h3>Địa điểm làm việc<a href="#"><i class="fa fa-close"></i></a></h3>

                    <div class="checkbox">
                        <label><input type="radio" name="radio-test" value="1"> San Francisco</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="radio" name="radio-test" value="2"> Sacramento</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="radio" name="radio-test" value="3"> Los Angeles</label>
                    </div><!-- /.checkbox -->

                    <a href="#" class="filter-stacked-show-more">Thêm...</a>

                    <h3>Trạng thái <a href="#"><i class="fa fa-close"></i></a></h3>

                    <div class="checkbox">
                        <label><input type="checkbox"> Mới nhất</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="checkbox"> Nổi bật</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="checkbox"> Gấp</label>
                    </div><!-- /.checkbox -->

                    <a href="#" class="filter-stacked-show-more">Thêm...</a>

                    <button type="submit" class="btn btn-secondary btn-block"><i class="fa fa-refresh"></i> Xóa bộ lọc</button>
                </form>
            </div><!-- /.filter-stacked -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->
</div><!-- /.container -->