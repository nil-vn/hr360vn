<div class="container">
    <div class="col-sm-9">
        <form method="get" action="?">
            <div class="row" id="basic-information">
                <div class="col-sm-7">
                    <div class="form-group">
                        <label form="form-register-photo">Ảnh đại diện</label>
                        <input type="file" name="form-register-photo" id="form-register-photo">
                    </div><!-- /.form-group-->
                </div><!-- /.col-* -->

                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Họ & tên đệm</label>
                        <input type="text" class="form-control">
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <label>Tên</label>
                        <input type="text" class="form-control">
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->
            </div><!-- /.row -->

            <h3 class="page-header" id="contact">Thông tin liên hệ</h3>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tỉnh/Thành phố</label>
                        <input type="text" class="form-control">
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Quận/Huyện</label>
                        <input type="text" class="form-control">
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Địa chỉ</label>
                        <input type="text" class="form-control">
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Điện thoại</label>
                        <input type="text" class="form-control">
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="text" class="form-control">
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Website</label>
                        <input type="text" class="form-control">
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->
            </div><!-- /.row -->

            <h3 class="page-header" id="biography">Tóm tắt cá nhân</h3>

            <div class="form-group">
                <textarea id="editor" class="form-control"></textarea>
            </div><!-- /.form-group -->

            <h3 class="page-header" id="experience">Kinh nghiệm làm việc <a href="#" class="btn btn-primary">Thêm +</a></h3>

            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Vị trí công việc</label>
                        <input type="text" class="form-control">
                    </div><!-- /.form-group -->

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Từ ngày</label>
                                <input type="text" class="form-control">
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Đến ngày</label>
                                <input type="text" class="form-control">
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->
                </div><!-- /.col-* -->

                <div class="col-sm-7">
                    <div class="form-group">
                        <label>Mô tả ngắn về công việc</label>
                        <textarea class="form-control" rows="5"></textarea>
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->
            </div><!-- /.row -->

            <h3 class="page-header" id="education">Học vấn <a href="#" class="btn btn-primary">Thêm +</a></h3>

            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Trường</label>
                        <input type="text" class="form-control">
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <label>Bằng cấp</label>
                        <input type="text" class="form-control">
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->

                <div class="col-sm-7">
                    <div class="form-group">
                        <label>Mô tả ngắn</label>
                        <textarea class="form-control" rows="5"></textarea>
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->
            </div><!-- /.row -->

            <hr>

            <div class="center">
                <button type="submit" class="btn btn-secondary btn-lg">Lưu hồ sơ</button>
            </div><!-- /.center -->
        </form>
    </div><!-- /.col-* -->

    <div class="col-sm-3">
        <div class="widget">
            <p>
                <a href="login.html" class="btn btn-block btn-secondary">Đăng nhập</a>
            </p>

            <p>
                <a href="#" class="btn btn-block btn-default">Xem trước</a>
            </p>
        </div><!-- /.widget -->

        <div class="widget">
            <h2>Điều hướng nhanh</h2>

            <ul class="nav">
                <li><a href="#basic-information"><span>1.</span> Basic Information</a></li>
                <li><a href="#contact"><span>2.</span> Contact</a></li>
                <li><a href="#biography"><span>3.</span> Biography</a></li>
                <li><a href="#experience"><span>4.</span> Experience</a></li>
                <li><a href="#education"><span>5.</span> Education </a></li>
            </ul>
        </div><!-- /.widget -->

        <div class="widget">
            <h2>Bạn đã có tài khoản chưa?</h2>

            <p>If you don't have an account and you are looking for new employees. Feel free to <a href="registration.html">create new registration</a> for companies with transparent pricing.</p>
        </div><!-- /.widget -->
    </div><!-- /.col-* -->
</div><!-- /.container -->