<div class="container">
    <div class="row">
        <div class="col-sm-9">
            <h2 class="page-header">Tìm thấy <strong><?php echo count($companies);?></strong> nhà tuyển dụng</h2>
            <div class="companies-list">
                <?php foreach($companies as $key => $company){ ?>
                <div class="companies-list-item">
                    <div class="companies-list-item-image">
                        <a href="#">
                            <img src="<?php echo $company['Company']['logo_url'];?>" alt="">
                        </a>
                    </div><!-- /.companies-list-item-image -->

                    <div class="companies-list-item-heading">
                        <h2><a href="/company/<?php echo $company['Company']['slug'];?>"><?php echo $company['Company']['company_name'];?></a></h2>
                        <h3><?php echo $company['Company']['location'];?></h3>
                    </div><!-- /.companies-list-item-heading -->

                    <div class="companies-list-item-count">
                        <a href="/company/<?php echo $company['Company']['slug'];?>#jobs"><?php echo count($company['Job']);?> vị trí đang tuyển</a>
                    </div><!-- /.positions-list-item-count -->

                    <div class="companies-list-item-rating">
                        <?php if(!empty($company['Company']['rating'])){
                            for($i = 1; $i <= $company['Company']['rating']; $i++){?>
                                <i class="fa fa-star"></i>
                        <?php }?>
                        <span class="companies-list-item-rating-count">/ <?php echo $company['Company']['rating_count'];?></span>
                        <?php }?>
                    </div><!-- /.companies-list-item-rating -->
                </div><!-- /.companies-list-item -->
                <?php }?>
            </div><!-- /.companies-list -->

             <div class="center">
                <ul class="pagination">
                    <li>
                        <a href="#">
                            <i class="fa fa-chevron-left"></i>
                        </a>
                    </li>

                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li class="active"><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#">
                            <i class="fa fa-chevron-right"></i>
                        </a>
                    </li>
                </ul>
            </div><!-- /.center -->
        </div><!-- /.col-* -->

        <div class="sidebar col-sm-3">
            <h2>Tin nổi bật</h2>
            <div class="positions-list-small">
            <?php foreach($hotjobs as $key => $hotjob){?>
                <div class="positions-list-small-item">
                    <h2><a href="/jobs/<?php echo $hotjob['Job']['slug'];?>"><?php echo $hotjob['Job']['job_title'];?></a></h2>
                    <h3><span><img src="<?php echo $hotjob['Company']['logo_url'];?>" alt=""></span> <?php echo $hotjob['Company']['company_name'];?> <br><?php echo $hotjob['Job']['location'];?></h3>
                </div>
            <?php }?>
            </div><!-- /.positions-list-small -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->
</div><!-- /.container -->