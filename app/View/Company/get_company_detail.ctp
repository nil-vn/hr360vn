<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <div class="company-card">
                <div class="company-card-image">
                    <?php if($company['Company']['service_type'] == 2){?>
                    <span>Nhà tuyển dụng hàng đầu</span>
                    <?php }?>
                    <a href="/company/<?php echo $company['Company']['slug'];?>">
                        <img src="<?php echo $company['Company']['logo_url'];?>" alt=""></a>
                    </a>
                </div><!-- /.company-card-image -->

                <div class="company-card-data">
                    <dl>
                        <dt>Website</dt>
                        <dd><a href="<?php echo $company['Company']['homepage_url'];?>"><?php echo $company['Company']['homepage_url'];?></a></dd>

                        <dt>E-mail</dt>
                        <dd><a href="mailto:<?php echo $company['Company']['email'];?>"><?php echo $company['Company']['email'];?></a></dd>

                        <dt>Điện thoại</dt>
                        <dd><?php echo $company['Company']['telephone'];?></dd>

                        <dt>Địa chỉ</dt>
                        <dd><?php echo $company['Company']['location'];?></dd>
                    </dl>
                </div><!-- /.company-card-data -->
            </div><!-- /.company-card -->

            <div class="widget">
                <ul class="social-links">
                    <li><a href="<?php echo $company['Company']['facebook_url'];?>"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                </ul>
            </div><!-- /.widget -->

            <div class="widget">
                <h2>Liên hệ</h2>

                <form method="get" action="?">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Subject">
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Your E-mail">
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <textarea class="form-control" rows="5" placeholder="Your Message"></textarea>
                    </div><!-- /.form-group -->

                    <button class="btn btn-secondary pull-right" type="submit">Gửi tin nhắn</button>
                </form>
            </div><!-- /.widget -->
        </div><!-- /.col-* -->

        <div class="col-sm-8">
            <div class="company-header">
                <h1><?php echo $company['Company']['company_name'];?></h1>

                <a href="#" class="btn btn-secondary">Yêu thích </a>
                <a href="#" class="btn btn-default">Nhận thông tin cập nhật</a>
                <a href="#" class="btn btn-default">Theo dõi</a>
            </div><!-- /.company-header -->

            <div class="company-stats">
                <div class="company-stat">
                    <span>Vị trí tuyển dụng</span>
                    <strong><?php echo count($company['Job']);?></strong>
                </div><!-- /.company-stat -->

                <div class="company-stat">
                    <span>Nhân viên</span>
                    <strong><?php echo $company['Company']['employees'];?></strong>
                </div><!-- /.company-stat -->

                <div class="company-stat">
                    <span>Người theo dõi</span>
                    <strong><?php echo $company['Company']['followers'];?></strong>
                </div><!-- /.company-stat -->
            </div><!-- /.company-stat -->

            <h3 class="page-header">Giới thiệu về công ty</h3>
            <div class="company-description">
                <?php echo $company['Company']['description'];?>
            </div>

            <h3 class="page-header" id="jobs">Các vị trí đang tuyển</h3>

            <div class="positions-list">
            <?php if(!empty(count($company['Job']))){?>
            <?php foreach($company['Job'] as $key => $job){?>
                <div class="positions-list-item">
                    <h2><a href="/jobs/<?php echo $job['slug'];?>"><?php echo $job['job_title'];?></a></h2>
                    <h3><span><img src="<?php echo $company['Company']['logo_url'];?>" alt=""></span> <?php echo $company['Company']['company_name'];?> <br></h3>

                    <div class="position-list-item-date"><?php echo date('d/m/Y',strtotime($job['available_to']));?></div><!-- /.position-list-item-date -->
                    <div class="position-list-item-action"><a href="#">Lưu công việc này</a></div><!-- /.position-list-item-action -->
                </div><!-- /.positions-list-item -->
            <?php }
            } else {?>
                <p>Hiện không có vị trí nào.</p>
            <?php }?>
            </div><!-- /.positions-list -->
        </div><!-- /.col-sm-8 -->
    </div><!-- /.row -->
</div><!-- /.container -->