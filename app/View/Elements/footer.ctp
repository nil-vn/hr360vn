<div class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <div class="footer-top-block">
                        <h2><i class="profession profession-logo"></i> Da Nang Jobs Portal</h2>

                        <p>
                            Fusce congue, risus et pulvinar cursus, orci arcu tristique lectus, sit amet placerat justo ipsum eu diam. Pellentesque tortor urna, pellentesque nec molestie eget, volutpat in arcu. Maecenas a lectus mollis.
                        </p>

                        <ul class="social-links">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                        </ul>
                    </div><!-- /.footer-top-block -->
                </div><!-- /.col-* -->

                <div class="col-sm-3 col-sm-offset-1">
                    <div class="footer-top-block">
                        <h2>Liên kết hữu ích</h2>

                        <ul>
                            <li><a href="#">Giới thiệu</a></li>
                            <li><a href="#">Hỗ trợ</a></li>
                            <li><a href="#">Điều khoản hoạt động</a></li>
                            <li><a href="#">Liên hệ</a></li>
                        </ul>
                    </div><!-- /.footer-top-block -->
                </div><!-- /.col-* -->

                <div class="col-sm-3">
                    <div class="footer-top-block">
                        <h2>Xu hướng tuyển dụng</h2>

                        <ul>
                            <li><a href="/jobs/senior-java-developer">Android Developer</a></li>
                            <li><a href="/jobs/senior-java-developer">Senior Teamleader</a></li>
                            <li><a href="/jobs/senior-java-developer">iOS Developer</a></li>
                            <li><a href="/jobs/senior-java-developer">Junior Tester</a></li>
                            <li><a href="/jobs/senior-java-developer">Full Stack Developer</a></li>
                        </ul>
                    </div><!-- /.footer-top-left -->
                </div><!-- /.col-* -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-top -->

    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-left">
                &copy; <a href="#">Da Nang Jobs Portal</a>, 2016 All rights reserved.
            </div><!-- /.footer-bottom-left -->

            <div class="footer-bottom-right">
                Created by <a href="http://byaviators.com">Aviators</a>. Premium themes and templates.
            </div><!-- /.footer-bottom-right -->
        </div><!-- /.container -->
    </div><!-- /.footer-bottom -->
</div><!-- /.footer -->