<div class="header">
    <div class="header-top">
        <div class="container">
            <div class="header-brand">
                <div class="header-logo">
                    <a href="/">
                        <i class="profession profession-logo"></i>
                        <span class="header-logo-text">Da Nang Jobs<span class="header-logo-highlight">.</span>Portal</span>
                    </a>
                </div><!-- /.header-logo-->

                <div class="header-slogan">
                    <span class="header-slogan-slash">/</span>
                    <span class="header-slogan-text">thành công trong tầm tay</span>
                </div><!-- /.header-slogan-->
            </div><!-- /.header-brand -->

            <ul class="header-actions nav nav-pills">
                <li><a href="/login">Đăng nhập</a></li>
                <li><a href="/registration">Đăng ký</a></li>
                <li><a href="/create-resume" class="primary">Tạo hồ sơ</a></li>
            </ul><!-- /.header-actions -->

            <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div><!-- /.container -->
    </div><!-- /.header-top -->

    <div class="header-bottom">
        <div class="container">
            <ul class="header-nav nav nav-pills collapse">
                <li class="active">
                    <a href="/">Trang chủ</a>
                </li>

                <li >
                    <a href="/companies">Nhà tuyển dụng</a>
                </li>

                <li >
                    <a href="/jobs">Vị trí tuyển dụng <i class="fa fa-chevron-down"></i></a>
                    <ul class="sub-menu">
                        <li><a href="/jobs/senior-java-developer">Nhân viên kinh doanh</a></li>
                        <li><a href="/jobs/senior-java-developer">Kỹ sư phần mềm</a></li>
                        <li><a href="/jobs/senior-java-developer">Hành chính - Nhân sự</a></li>
                        <li><a href="/jobs/senior-java-developer">Tài chính - Ngân hàng</a></li>
                    </ul><!-- /.sub-menu -->
                </li>

                <li >
                    <a href="#">Tư vấn<i class="fa fa-chevron-down"></i></a>

                    <ul class="sub-menu">
                        <li><a href="/create-resume">Hỏi đáp</a></li>
                    </ul><!-- /.sub-menu -->
                </li>
            </ul>

            <div class="header-search hidden-sm">
                <form method="get" action="/search">
                    <input type="text" name="keyword" class="form-control" placeholder="Tìm kiếm ...">
                </form>
            </div><!-- /.header-search -->
        </div><!-- /.container -->
    </div><!-- /.header-bottom -->
</div><!-- /.header -->