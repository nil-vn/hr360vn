<?php
	$successFlash = $this->Session->flash('success');
	$errorFlash = $this->Session->flash('error');
	$infoFlash = $this->Session->flash('info');
	$warningFlash = $this->Session->flash('warning');
?>

<?php if($successFlash){?>
<div class="alert alert-success alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php echo $successFlash;?>
</div>
<?php } elseif($errorFlash){?>
<div class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php echo $errorFlash;?>
</div>
<?php } elseif($infoFlash){?>
<div class="alert alert-info alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php echo $infoFlash;?>
</div>
<?php }elseif($warningFlash){?>
<div class="alert alert-warning alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php echo $warningFlash;?>
</div>
<?php }?>