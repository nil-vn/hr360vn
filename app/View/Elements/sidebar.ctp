<!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search..."/>
        <span class="input-group-btn">
          <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">CONTENT MANAGEMENT</li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-file"></i> <span>Job Categories</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="/admin/category"><i class="fa fa-folder-open"></i>All Categories</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-file"></i> <span>Available Jobs</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="/admin/jobs"><i class="fa fa-folder-open"></i>All Jobs</a></li>
          <li><a href="/admin/jobs/add"><i class="fa fa-folder-open"></i>Add new job</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-file"></i> <span>Candidates</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="/admin/jobs"><i class="fa fa-folder-open"></i>Document</a></li>
          <li><a href="/admin/jobs/add"><i class="fa fa-folder-open"></i>Report</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="/admin/company">
          <i class="fa fa-file"></i> <span>Company</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-users"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="/admin"><i class="fa fa-files-o"></i> All users</a></li>
        </ul>
      </li>
      <li class="header">MORE ACTIONS</li>
      <li>
        <a href="#"><i class="fa fa-cogs text-red"></i> <span>Setting</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li><a href="/admin/setting/category"><i class="fa fa-folder-open"></i> Categories</a></li>
        </ul>
      </li>
      <li><a href="#"><i class="fa fa-sign-out text-red"></i> <span>Logout</span></a></li>
      <li><a href="#"><i class="fa fa-info-circle text-red"></i> <span>Information</span></a></li>
    </ul>
  </section>