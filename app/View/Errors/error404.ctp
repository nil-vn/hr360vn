<div class="container">
	<div class="inner404">
		<h2><i class="fa fa-warning"></i> <?php echo __('Oops! Page not found.');?></h2>
		<p><?php echo __('We could not find the page you were looking for. Meanwhile, you may return to');?> <a href="/" style="cursor: pointer;"><?php echo __('homepage');?></a> <?php echo __('or try using the');?> <a href="/search" style="cursor: pointer;"><?php echo __('search form');?></a>.</p>
	</div>
</div>