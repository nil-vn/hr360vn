<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppHelper', 'View/Helper');
App::uses('Report', 'Model');
/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class MenuHelper extends AppHelper {

	public function __construct(View $view, $settings = array()) {
		parent::__construct($view, $settings);
	}

	public function listPrmUnits() {
		$model 		= ClassRegistry::init('Report');
		$p_units 	= $model->getPrimaryUnits();
		foreach ($p_units as $key => $value) {
			echo '<li><a href="/admin/prm_units/'.$value['reports']['MA_TRAM'].'"><i class="fa fa-bar-chart"></i> '.$value['reports']['TEN_TRAM'].'</a></li>';
		}
	}

	/**
	 * TODO:
	 * List prmUnits from DB with setting: hide or show in menu
	 * -> Create setting page
	 */
}
