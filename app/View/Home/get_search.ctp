<div class="container">
	<h2 class="page-header"><strong><?php echo count($jobs);?></strong> công việc được tìm thấy</h2>
    <div class="row">
        <div class="col-sm-3">
            <div class="filter-stacked">
                <form method="post" action="?">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Từ khóa">
                    </div>

                    <h3>Lương <a href="#"><i class="fa fa-close"></i></a></h3>

                    <div class="checkbox">
                        <label><input type="radio" name="radio-test" value="1">Đến 5 triệu</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="radio" name="radio-test" value="2">5 - 10 triệu</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="radio" name="radio-test" value="3">Trên 10 triệu</label>
                    </div><!-- /.checkbox -->

                    <h3>Loại công việc <a href="#"><i class="fa fa-close"></i></a></h3>

                    <div class="checkbox">
                        <label><input type="checkbox"> Toàn thời gian</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="checkbox"> Bán thời gian</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="checkbox"> Các dự án 1 lần</label>
                    </div><!-- /.checkbox -->

                    <h3>Địa điểm làm việc<a href="#"><i class="fa fa-close"></i></a></h3>

                    <div class="checkbox">
                        <label><input type="radio" name="radio-test" value="1"> San Francisco</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="radio" name="radio-test" value="2"> Sacramento</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="radio" name="radio-test" value="3"> Los Angeles</label>
                    </div><!-- /.checkbox -->

                    <a href="#" class="filter-stacked-show-more">Thêm...</a>

                    <h3>Trạng thái <a href="#"><i class="fa fa-close"></i></a></h3>

                    <div class="checkbox">
                        <label><input type="checkbox"> Mới nhất</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="checkbox"> Nổi bật</label>
                    </div><!-- /.checkbox -->

                    <div class="checkbox">
                        <label><input type="checkbox"> Tuyển gấp</label>
                    </div><!-- /.checkbox -->

                    <a href="#" class="filter-stacked-show-more">Thêm...</a>

                    <button type="submit" class="btn btn-secondary btn-block"><i class="fa fa-refresh"></i> Xóa bộ lọc</button>
                </form>
            </div><!-- /.filter-stacked -->
        </div><!-- /.col-* -->

        <div class="col-sm-7">
            <div class="positions-list">
            <?php foreach($jobs as $key => $job){?>
                <div class="positions-list-item">
                    <h2>
                        <a href="/jobs/<?php echo $job['Job']['slug'];?>"><?php echo $job['Job']['job_title'];?></a>
                        <?php
                        $span = '';
                        if($job['Job']['is_hot']){
                            $span = '<span>Hot</span>';
                        } elseif($job['Job']['is_new']){
                            $span = '<span>New</span>';
                        } elseif($job['Job']['is_urgent']){
                            $span = '<span>Urgent</span>';
                        }
                        echo $span;
                        ?>
                    </h2>
                    <h3><span><img src="<?php echo $job['Company']['logo_url'];?>" alt=""></span> <?php echo $job['Job']['location'];?><br></h3>

                    <div class="position-list-item-date"><?php echo date('d/m/Y', strtotime($job['Job']['available_to']));?></div><!-- /.position-list-item-date -->
                    <div class="position-list-item-action"><a href="#">Lưu công việc này</a></div><!-- /.position-list-item-action -->
                </div><!-- /.position-list-item -->
            <?php }?>
            </div><!-- /.positions-list -->

             <div class="center">
            	<ul class="pagination">
            		<li>
            			<a href="#">
            				<i class="fa fa-chevron-left"></i>
            			</a>
            		</li>

            		<li><a href="#">1</a></li>
            		<li><a href="#">2</a></li>
            		<li class="active"><a href="#">3</a></li>
            		<li><a href="#">4</a></li>
            		<li><a href="#">5</a></li>
            		<li>
            			<a href="#">
            				<i class="fa fa-chevron-right"></i>
            			</a>
            		</li>
            	</ul>
            </div><!-- /.center -->
        </div><!-- /.col-* -->

        <div class="col-sm-2">
            <div class="banner-wrapper">
                <a href="#">
                    <img src="/assets/img/tmp/banner-120x600.png" alt="">
                </a>
            </div><!-- /.banner-wrapper -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->
</div><!-- /.container -->