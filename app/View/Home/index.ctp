<div class="hero-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <h1>Tìm kiếm cơ hội với Đà Nẵng Jobs .Portal</h1>
                <h2>Với sự tin tưởng và kiểm định bởi các công ty hàng đầu sử dụng hệ thống của chúng tôi để tìm kiếm các ứng viên tốt nhất, hãy bắt đầu sự thay đổi của bạn chỉ với 1 cú nhấp chuột ở "Tạo hồ sơ"</h2>

                <a href="create-resume.html" class="hero-content-action">Tạo hồ sơ</a>
            </div><!-- /.col-* -->

            <div class="col-sm-6 col-md-5 col-md-offset-1">
                <div class="hero-content-carousel">
                    <h2>Có gì mới?</h2>

                    <ul class="cycle-slideshow vertical"
                        data-cycle-fx="carousel"
                        data-cycle-slides="li"
                        data-cycle-carousel-visible="7"
                        data-cycle-carousel-vertical="true">
                        <li><a href="/jobs/senior-java-developer">Dropbox</a> đang tìm kiếm UX/UI designer.</li>
                        <li>Python Developer <a href="#">John Doe</a>.</li>
                        <li><a href="/jobs/senior-java-developer">IT consultant</a> đang cần bởi <a href="/company/dropbox">Twitter</a>.</li>
                        <li>Project manager được săn lùng bởi <a href="/company/dropbox">e-shop portal</a>.</li>
                        <li><a href="/company/dropbox">Mark Peterson</a> cần ai đó thiết kế website của anh ấy.</li>
                        <li><a href="/company/dropbox">Facebook</a> đang tìm kiếm <a href="/jobs/senior-java-developer">các testers</a>.</li>
                        <li><a href="/company/dropbox">Instagram</a> cần giúp đỡ để thực hiện các API mới.</li>
                        <li>Project manager được săn lùng bởi <a href="/company/dropbox">e-shop portal</a>.</li>
                        <li><a href="/company/dropbox">Mark Peterson</a> cần ai đó thiết kế website của anh ấy.</li>
                        <li><a href="/company/dropbox">Facebook</a> đang tìm kiếm <a href="/jobs/senior-java-developer">các testers</a>.</li>
                        <li><a href="/company/dropbox">Instagram</a> cần giúp đỡ để thực hiện các API mới.</li>
                    </ul>

                    <a href="/jobs" class="hero-content-show-all">Xem tất cả công việc</a>
                </div><!-- /.hero-content-content -->
            </div><!-- /.col-* -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.hero-content -->


<div class="stats">
    <div class="container">
        <div class="row">
            <div class="stat-item col-sm-4" data-to="123012">
                <strong id="stat-item-1">0</strong>
                <span>Công việc mới hàng ngày</span>
            </div><!-- /.col-* -->

            <div class="stat-item col-sm-4" data-to="187432">
                <strong id="stat-item-2">0</strong>
                <span>Hồ sơ ứng viên</span>
            </div><!-- /.col-* -->

            <div class="stat-item col-sm-4" data-to="140312">
                <strong id="stat-item-3">0</strong>
                <span>Nhà tuyển dụng</span>
            </div><!-- /.col-* -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.stats -->


<div class="container" id="search">
    <div class="filter">
        <h2>Tìm kiếm công việc</h2>
        <form method="get" action="/search">
            <div class="row">
                <div class="form-group col-sm-5">
                    <select class="form-control" title="Chọn địa điểm" name="l">
                        <option value="newyork">New York City, NY</option>
                        <option value="ca">Palo Alto, CA</option>
                    </select>
                </div><!-- /.form-group -->

                <div class="form-group col-sm-5">
                    <select class="form-control" title="Chọn ngành nghề" name="c">
                        <option value="it">Công nghệ thông tin</option>
                        <option value="bank">Tài chính - Ngân hàng</option>
                        <option value="hr">Hành chính - Nhân sự</option>
                        <option value="finance">Kế toán - Kiểm toán</option>
                    </select>
                </div><!-- /.form-group -->

                <div class="form-group col-sm-2">
                    <button type="submit" class="btn btn-block btn-secondary">Tìm kiếm</button>
                </div><!-- /.form-group -->
            </div><!-- /.row -->

            <ul class="filter-list">
                <li><a href="/jobs">Công nghệ thông tin <span class="filter-list-count">(1704)</span></a></li>
                <li><a href="/jobs">Tài chính/Ngân hàng <span class="filter-list-count">(1215)</span></a></li>
                <li><a href="/jobs">Hành chính/Nhân sự <span class="filter-list-count">(904)</span></a></li>
                <li><a href="/jobs">Kế toán/Kiểm toán <span class="filter-list-count">(2744)</span></a></li>
                <li><a href="/jobs">Bán hàng <span class="filter-list-count">(904)</span></a></li>
                <li><a href="/jobs">Cơ khí <span class="filter-list-count">(1804)</span></a></li>
                <li><a href="/jobs">Điện/Điện tử <span class="filter-list-count">(875)</span></a></li>
                <li><a href="/jobs">Kiến trúc/Xây dựng <span class="filter-list-count">(546)</span></a></li>
                <li><a href="/jobs">Mỹ thuật/Đồ họa <span class="filter-list-count">(7895)</span></a></li>
                <li><a href="/jobs">Thiết kế nội thất <span class="filter-list-count">(4562)</span></a></li>
                <li><a href="/jobs">Dịch vụ tư vấn <span class="filter-list-count">(1658)</span></a></li>
                <li><a href="/jobs">Bảo hiểm <span class="filter-list-count">(1354)</span></a></li>
                <li><a href="/jobs">Bảo trì/Sửa chữa <span class="filter-list-count">(6988)</span></a></li>
                <li><a href="/jobs">Bảo vệ <span class="filter-list-count">(1235)</span></a></li>
                <li><a href="/jobs">Biên/Phiên dịch <span class="filter-list-count">(4563)</span></a></li>
                <li><a href="/jobs">Dược <span class="filter-list-count">(1549)</span></a></li>
                <li><a href="/jobs">Marketing <span class="filter-list-count">(432)</span></a></li>
                <li><a href="/jobs">Kho vận/Logictis <span class="filter-list-count">(4123)</span></a></li>
                <li><a href="/jobs">Pháp lý <span class="filter-list-count">(923)</span></a></li>
                <li><a href="/jobs">QA/QC <span class="filter-list-count">(909)</span></a></li>
            </ul>

            <hr>

            <div class="filter-actions">
                <a href="/candidates">Tất cả ứng viên</a> <span class="filter-separator">/</span>
                <a href="/jobs">Tất cả công việc</a> <span class="filter-separator">/</span>
                <a href="/companies">Tất cả công ty</a>
            </div><!-- /.filter -->
        </form>
    </div><!-- /.filter -->


    <div class="row">
        <div class="col-sm-6">
            <h2 class="page-header">Các nhà tuyển dụng hàng đầu</h2>
            <div class="companies-list">
            <?php foreach($top_companies as $key => $top_company){?>
                <div class="companies-list-item">
                    <div class="companies-list-item-image">
                        <a href="/company/<?php echo $top_company['Company']['slug'];?>">
                            <img src="<?php echo $top_company['Company']['logo_url'];?>" alt="">
                        </a>
                    </div><!-- /.companies-list-item-image -->
                    <div class="companies-list-item-heading">
                        <h2><a href="/company/<?php echo $top_company['Company']['slug'];?>"><?php echo $top_company['Company']['company_name'];?></a></h2>
                        <h3><?php echo $top_company['Company']['location'];?></h3>
                    </div><!-- /.companies-list-item-heading -->
                    <div class="companies-list-item-count">
                        <a href="/company/<?php echo $top_company['Company']['slug'];?>#jobs"><?php echo count($top_company['Job']);?> vị trí đang tuyển</a>
                    </div><!-- /.positions-list-item-count -->
                </div><!-- /.companies-list-item -->
            <?php }?>
            </div><!-- /.companies-list -->
        </div><!-- /.col-* -->

        <div class="col-sm-6">
            <h2 class="page-header">Các vị trí tuyển dụng mới nhất</h2>
            <div class="positions-list">
            <?php foreach($new_jobs as $key => $new_job){?>
                <div class="positions-list-item">
                    <h2>
                        <a href="/jobs/<?php echo $new_job['Job']['slug'];?>"><?php echo $new_job['Job']['job_title'];?></a>
                    </h2>
                    <h3><span><img src="<?php echo $new_job['Company']['logo_url'];?>" alt=""></span> <?php echo $new_job['Job']['location'];?> <br></h3>
                    <div class="position-list-item-date"><?php echo date('d/m/Y', strtotime($new_job['Job']['available_to']));?></div><!-- /.position-list-item-date -->
                    <div class="position-list-item-action"><a href="#">Lưu công việc này</a></div><!-- /.position-list-item-action -->
                </div><!-- /.position-list-item -->
            <?php }?>
            </div><!-- /.positions-list -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->



    <div class="panels-highlighted">
        <div class="row">
            <div class="panel-highlighted-wrapper col-sm-6">
                <div class="panel-highlighted-inner panel-highlighted-secondary">
                    <h2>Bạn đang tìm kiếm nhân tài?</h2>
                    <p>
                        Vivamus congue rhoncus sem et placerat. Fusce nec nunc lobortis lorem ultrices facilisis. Ut dapibus blandit nunc, et consectetur dui.
                    </p>
                    <a href="registration.html" class="btn btn-white">Đăng nhập cho Nhà tuyển dụng</a>
                </div><!-- /.panel-inner -->
            </div><!-- /.panel-wrapper -->

            <div class="panel-highlighted-wrapper col-sm-6">
                <div class="panel-highlighted-inner panel-highlighted-primary panel">
                    <h2>Bạn đang cần một công việc phù hợp?</h2>
                    <p>
                        Vivamus congue rhoncus sem et placerat. Fusce nec nunc lobortis lorem ultrices facilisis. Ut dapibus blandit nunc, et consectetur dui.
                    </p>
                    <a href="registration.html" class="btn btn-white">Đăng nhập cho Ứng viên</a>
                </div><!-- /.panel-inner -->
            </div><!-- /.panel-wrapper -->
        </div><!-- /.row-->
    </div><!-- /.panels -->

    <div class="panels">
        <div class="row">
            <div class="panel-wrapper col-sm-4">
                <div class="panel-inner">
                    <h2>Tại sao lại chọn chúng tôi?</h2>
                    <ul>
                        <li><i class="fa fa-check"></i> Quy trình chọn lọc kỹ lưỡng, đa chiều </li>
                        <li><i class="fa fa-check"></i> Đội ngũ nhân viên giàu kinh nghiệm</li>
                        <li><i class="fa fa-check"></i> Độ phủ sóng rộng</li>
                    </ul>
                </div><!-- /.panel-inner -->
            </div><!-- /.panel-wrapper -->

            <div class="panel-wrapper col-sm-4">
                <div class="panel-inner">
                    <h2>Các giá trị gia tăng?</h2>
                    <ul>
                        <li><i class="fa fa-check"></i> Tự động hóa công việc</li>
                        <li><i class="fa fa-check"></i> Kiểm soát dễ dàng</li>
                        <li><i class="fa fa-check"></i> Hỗ trợ mọi lúc bạn cần</li>
                    </ul>
                </div><!-- /.panel-inner -->
            </div><!-- /.panel-wrapper -->

            <div class="panel-wrapper col-sm-4">
                <div class="panel-inner">
                    <h2>Chi phí?</h2>
                    <p>Gần như miễn phí</p>
                    <a href="pricing.html" class="panel-show-more">Tham khảo</a>
                </div><!-- /.panel-inner -->
            </div><!-- /.panel-wrapper -->
        </div><!-- /.row -->
    </div><!-- /.panels -->

    <div class="block background-secondary fullwidth candidate-title">
        <div class="page-title">
            <h2>Tìm kiếm tài năng sáng giá nhất</h2>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <p>
                        Donec tincidunt felis quam, eu tempus purus finibus in. Curabitur hendrerit, odio in viverra interdum, lorem velit scelerisque ipsum, a sagittis ligula leo in dolor. Etiam vestibulum.
                    </p>
                </div><!-- /.col-* -->
            </div><!-- /.row -->
        </div><!-- /.page-title -->
    </div><!-- /.fullwidth -->

    <div class="row mt-60">
        <div class="candidate-boxes">
            <div class="col-sm-3 col-md-2">
                <div class="candidate-box">
                    <div class="candidate-box-image">
                        <a href="resume.html">
                            <img src="<?php echo $this->webroot;?>assets/img/tmp/resume-1.jpg" alt="Peter Ruck">
                        </a>
                    </div><!-- /.candidate-box-image -->

                    <div class="candidate-box-content">
                        <h2>Peter Ruck</h2>
                        <h3>Java Developer</h3>
                    </div><!-- /.candidate-box-content -->
                </div><!-- /.candidate-box -->
            </div><!-- /.col-* -->



            <div class="col-sm-3 col-md-2">
                <div class="candidate-box">
                    <div class="candidate-box-image">
                        <a href="resume.html">
                            <img src="<?php echo $this->webroot;?>assets/img/tmp/resume-2.jpg" alt="Linda Young">
                        </a>
                    </div><!-- /.candidate-box-image -->

                    <div class="candidate-box-content">
                        <h2>Linda Young</h2>
                        <h3>PR Manager</h3>
                    </div><!-- /.candidate-box-content -->
                </div><!-- /.candidate-box -->
            </div><!-- /.col-* -->



            <div class="col-sm-3 col-md-2">
                <div class="candidate-box">
                    <div class="candidate-box-image">
                        <a href="resume.html">
                            <img src="<?php echo $this->webroot;?>assets/img/tmp/resume-3.jpg" alt="Britney Doe">
                        </a>
                    </div><!-- /.candidate-box-image -->

                    <div class="candidate-box-content">
                        <h2>Britney Doe</h2>
                        <h3>Data Mining</h3>
                    </div><!-- /.candidate-box-content -->
                </div><!-- /.candidate-box -->
            </div><!-- /.col-* -->



            <div class="col-sm-3 col-md-2">
                <div class="candidate-box">
                    <div class="candidate-box-image">
                        <a href="resume.html">
                            <img src="<?php echo $this->webroot;?>assets/img/tmp/resume-4.jpg" alt="Andrew Man">
                        </a>
                    </div><!-- /.candidate-box-image -->

                    <div class="candidate-box-content">
                        <h2>Andrew Man</h2>
                        <h3>Python Developer</h3>
                    </div><!-- /.candidate-box-content -->
                </div><!-- /.candidate-box -->
            </div><!-- /.col-* -->



            <div class="col-sm-3 col-md-2">
                <div class="candidate-box">
                    <div class="candidate-box-image">
                        <a href="resume.html">
                            <img src="<?php echo $this->webroot;?>assets/img/tmp/resume.jpg" alt="Elliot Sarah Scott">
                        </a>
                    </div><!-- /.candidate-box-image -->

                    <div class="candidate-box-content">
                        <h2>Elliot Sarah Scott</h2>
                        <h3>Data Analytist</h3>
                    </div><!-- /.candidate-box-content -->
                </div><!-- /.candidate-box -->
            </div><!-- /.col-* -->



            <div class="col-sm-3 col-md-2">
                <div class="candidate-box">
                    <div class="candidate-box-image">
                        <a href="resume.html">
                            <img src="<?php echo $this->webroot;?>assets/img/tmp/resume-1.jpg" alt="Peter Ruck">
                        </a>
                    </div><!-- /.candidate-box-image -->

                    <div class="candidate-box-content">
                        <h2>Peter Ruck</h2>
                        <h3>Java Developer</h3>
                    </div><!-- /.candidate-box-content -->
                </div><!-- /.candidate-box -->
            </div><!-- /.col-* -->

        </div><!-- /.candidate-boxes -->
    </div><!-- /.row -->


    <div class="partners">
        <a href="#">
            <img src="<?php echo $this->webroot;?>assets/img/tmp/partner-1.jpg" alt="">
        </a>

        <a href="#">
            <img src="<?php echo $this->webroot;?>assets/img/tmp/partner-2.jpg" alt="">
        </a>

        <a href="#">
            <img src="<?php echo $this->webroot;?>assets/img/tmp/partner-3.jpg" alt="">
        </a>

        <a href="#">
            <img src="<?php echo $this->webroot;?>assets/img/tmp/partner-4.jpg" alt="">
        </a>

        <a href="#">
            <img src="<?php echo $this->webroot;?>assets/img/tmp/partner-5.jpg" alt="">
        </a>
    </div><!-- /.partners -->

</div><!-- /.container -->