<div class="container">
	<div class="row">
		<div class="col-sm-8">
			<div class="position-header">
				<h1><?php
					echo $job['Job']['job_title'];
					$span = '';
					if($job['Job']['is_hot']){
						$span = '<span>Hot</span>';
					} elseif($job['Job']['is_new']){
						$span = '<span>New</span>';
					} elseif($job['Job']['is_urgent']){
						$span = '<span>Urgent</span>';
					}
					echo $span;?></h1>

				<h2>
					<span class="position-header-company-image">
						<a href="/company/<?php echo $job['Company']['slug'];?>">
							<img src="<?php echo $job['Company']['logo_url'];?>" alt="">
						</a>
					</span>

					<a href="/company/<?php echo $job['Company']['slug'];?>"><?php echo $job['Company']['company_name'];?></a>
				</h2>
			</div><!-- /.position-header -->

			<div class="position-general-information">
				<dl>
					<dt>Địa điểm làm việc </dt>
					<dd><?php echo $job['Job']['location'];?></dd>

					<dt>Đăng ngày</dt>
					<dd><?php echo date('d/m/Y', strtotime($job['Job']['created']));?></dd>

					<dt>Loại hình công việc </dt>
					<dd><?php
					if($job['Job']['type'] == 1){
						echo 'Full-time';
					} elseif($job['Job']['type'] == 2) {
						echo 'Part-time';
					} else{
						echo '1 times projects';
					}
					?></dd>

					<dt>Lương </dt>
					<dd><?php echo $job['Job']['salary'].' Triệu đồng';?></dd>

					<dt>Mã công việc</dt>
					<dd>#<?php echo $job['Job']['job_group_id'];?></dd>
				</dl>
			</div><!-- /.position-general-information -->

			<h3 class="page-header">Mô tả công việc, quyền hạn và trách nhiệm</h3>
			<div class="description-detail">
				<?php echo $job['Job']['job_description'];?>
			</div>
			<h3 class="page-header">Các phúc lợi khác</h3>
			<div class="benefit-detail">
				<?php echo $job['Job']['benefit'];?>
			</div>
			<h3 class="page-header">Yêu cầu chung</h3>
			<div class="requires-detail">
				<?php echo $job['Job']['required'];?>
			</div>
		</div><!-- /.col-* -->

		<div class="col-sm-4">
			<div class="company-card">
				<div class="company-card-image">
					<?php if($job['Company']['service_type'] == '2'){?>
					<span>Nhà tuyển dụng hàng đầu</span>
					<?php }?>
					<a href="/company/<?php echo $job['Company']['slug'];?>">
						<img src="<?php echo $job['Company']['logo_url'];?>" alt=""></a>
					</a>
				</div><!-- /.company-card-image -->
				<div class="company-card-data">
					<dl>
						<dt>Website</dt>
						<dd><a href="<?php echo $job['Company']['homepage_url'];?>"><?php echo $job['Company']['homepage_url'];?></a></dd>

						<dt>E-mail</dt>
						<dd><a href="mailto:<?php echo $job['Company']['email'];?>"><?php echo $job['Company']['email'];?></a></dd>

						<dt>Điện thoại </dt>
						<dd><?php echo $job['Company']['telephone'];?></dd>

						<dt>Địa chỉ </dt>
						<dd><?php echo $job['Company']['location'];?></dd>
					</dl>
				</div><!-- /.company-card-data -->
			</div><!-- /.company-card -->

			<div class="widget">
				<h2>Ứng tuyển ngay bây giờ</h2>

				<form method="get" action="?">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Subject">
					</div><!-- /.form-group -->

					<div class="form-group">
						<input type="text" class="form-control" placeholder="Your E-mail">
					</div><!-- /.form-group -->

					<div class="form-group">
						<textarea class="form-control" rows="5" placeholder="Your Message"></textarea>
					</div><!-- /.form-group -->

					<button class="btn btn-secondary pull-right" type="submit">Nộp đơn</button>
				</form>
			</div><!-- /.widget -->
		</div><!-- /.col-* -->
	</div><!-- /.row -->
</div><!-- /.container -->