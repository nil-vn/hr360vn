<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <!-- General -->
    <meta name="description" content="DaNangJobsPortal là cổng thông tin tuyển dụng đáng tin cậy, là cầu nối doanh nghiệp và người lao động">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@DaNangJobsPortal">
    <meta name="twitter:title" content="DaNangJobsPortal: Cầu nối doanh nghiệp và người lao động">
    <meta name="twitter:creator" content="@DaNangJobsPortal">
    <meta name="twitter:description" content="DaNangJobsPortal là cổng thông tin tuyển dụng đáng tin cậy, là cầu nối doanh nghiệp và người lao động">
    <meta name="twitter:image:src" conteets"http://128.199.151.194/assets/thumb.jpg">
    <!-- Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://128.199.151.194/">
    <meta property="og:title" content="DaNangJobsPortal: Cầu nối doanh nghiệp và người lao động">
    <meta property="og:description" content="DaNangJobsPortal là cổng thông tin tuyển dụng đáng tin cậy, là cầu nối doanh nghiệp và người lao động">
    <meta property="og:site_name" content="DaNangJobsPortal">
    <meta property="og:image" content="http://128.199.151.194/assets/thumb.jpg">

    <meta name="robots" content="noodp">
    <!-- Google Plus -->
    <meta itemprop="name" content="DaNangJobsPortal: Cầu nối doanh nghiệp và người lao động">
    <meta itemprop="description" content="DaNangJobsPortal là cổng thông tin tuyển dụng đáng tin cậy, là cầu nối doanh nghiệp và người lao động">
    <meta itemprop="image" content="http://128.199.151.194/assets/thumb.jpg">
    <!-- Canonical URL -->
    <link rel="canonical" href="http://128.199.151.194/">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" type="text/css">
    <link href="<?php echo $this->webroot;?>assets/fonts/profession/style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $this->webroot;?>assets/libraries/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $this->webroot;?>assets/libraries/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $this->webroot;?>assets/libraries/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $this->webroot;?>assets/libraries/bootstrap-wysiwyg/bootstrap-wysiwyg.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $this->webroot;?>assets/css/profession-blue-navy.css" rel="stylesheet" type="text/css" id="style-primary">
    <link href="<?php echo $this->webroot;?>assets/css/common.css" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->webroot;?>assets/favicon.png">

    <title>Da Nang Jobs Portal</title>
</head>


<body class="hero-content-dark footer-dark">

<div class="page-wrapper">
    <div class="header-wrapper">
        <?php echo $this->element('nav');?>
    </div><!-- /.header-wrapper-->


    <div class="main-wrapper">
        <div class="main">
            <?php echo $this->fetch('content');?>
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

    <div class="footer-wrapper">
        <?php echo $this->element('footer');?>
    </div><!-- /.footer-wrapper -->

</div><!-- /.page-wrapper -->

<div class="action-bar">
    <div class="action-bar-content">
        <div class="action-bar-chapter">
            <strong>Chọn layout</strong>

            <ul>
                <li><a href="#" data-action="layout-wide" class="active">Wide</a></li><li><a href="#" data-action="layout-boxed">Boxed</a></li>
            </ul>
        </div><!-- /.action-bar-chapter -->

        <div class="action-bar-chapter">
            <strong>Header Style</strong>

            <ul>
                <li><a href="#" class="active" data-action="header-light">Light</a></li><li><a href="#" data-action="header-dark">Dark</a></li>
            </ul>
        </div><!-- /.action-bar-chapter -->

        <div class="action-bar-chapter">
            <strong>Navigation Style</strong>

            <ul>
                <li><a href="#" data-action="navigation-light">Light</a></li><li><a href="#" class="active" data-action="navigation-dark">Dark</a></li>
            </ul>
        </div><!-- /.action-bar-chapter -->

        <div class="action-bar-chapter">
            <strong>Hero Content Style</strong>

            <ul>
                <li><a href="#" data-action="hero-content-light">Light</a></li><li><a href="#" class="active" data-action="hero-content-dark">Dark</a></li>
            </ul>
        </div><!-- /.action-bar-chapter -->

        <div class="action-bar-chapter">
            <strong>Footer Style</strong>

            <ul>
                <li><a href="#" data-action="footer-light">Light</a></li><li><a href="#"  class="active" data-action="footer-dark">Dark</a></li>
            </ul>
        </div><!-- /.action-bar-chapter -->

        <div class="action-bar-chapter">
            <strong>Color Combination</strong>

            <table>
                <tr>
                    <td><a href="<?php echo $this->webroot;?>assets/css/profession-purple-red.css">Purple / Red</a></td>
                    <td><a href="<?php echo $this->webroot;?>assets/css/profession-blue-cyan.css">Blue / Cyan</a></td>
                </tr>

                <tr>
                    <td><a href="<?php echo $this->webroot;?>assets/css/profession-gray-orange.css">Gray / Orange</a></td>
                    <td><a href="<?php echo $this->webroot;?>assets/css/profession-black-green.css" class="active">Black / Green</a></td>
                </tr>

                <tr>
                    <td><a href="<?php echo $this->webroot;?>assets/css/profession-blue-navy.css">Blue / Navy</a></td>
                    <td></td>
                </tr>
            </table>
        </div><!-- /.action-bar-chapter -->
    </div><!-- /.action-bar-content -->

    <div class="action-bar-title">Tùy biến giao diện <i class="fa fa-ellipsis-v"></i></div><!-- /.action-bar-title -->
</div><!-- /.action-bar -->


<script type="text/javascript" src="<?php echo $this->webroot;?>assets/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>assets/js/jquery.ezmark.js"></script>

<script type="text/javascript" src="<?php echo $this->webroot;?>assets/libraries/bootstrap-sass/javascripts/bootstrap/collapse.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>assets/libraries/bootstrap-sass/javascripts/bootstrap/dropdown.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>assets/libraries/bootstrap-sass/javascripts/bootstrap/tab.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>assets/libraries/bootstrap-sass/javascripts/bootstrap/transition.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>assets/libraries/bootstrap-fileinput/js/fileinput.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>assets/libraries/bootstrap-select/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>assets/libraries/bootstrap-wysiwyg/bootstrap-wysiwyg.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>assets/libraries/cycle2/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>assets/libraries/cycle2/jquery.cycle2.carousel.min.js"></script>

<script type="text/javascript" src="<?php echo $this->webroot;?>assets/libraries/countup/countup.min.js"></script>

<script type="text/javascript" src="<?php echo $this->webroot;?>assets/js/profession.js"></script>


</body>
</html>
