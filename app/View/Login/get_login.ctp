<div class="container">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
			<form method="get" action="?">
				<div class="form-group">
					<label for="form-login-username">Tên đăng nhập</label>
					<input type="text" class="form-control" id="form-login-username">
				</div><!-- /.form-group -->

				<div class="form-group">
					<label for="form-login-password">Mật khẩu</label>
					<input type="password" class="form-control" id="form-login-password">
				</div><!-- /.form-group -->

				<div class="checkbox">
					<label><input type="checkbox"> Giữ trạng thái đăng nhập</label>

					<a href="#" class="link-not-important pull-right">Quên mật khẩu?</a>
				</div><!-- /.checkbox -->

				<div class="form-group">
					<button type="submit" class="btn btn-secondary btn-block">Đăng nhập</button>
				</div><!-- /.form-group -->

				<hr>

				<div class="row">
					<div class="col-xs-6 col-sm-6">
						<a href="#" class="btn btn-facebook btn-block"><i class="fa fa-facebook"></i> Facebook</a>
					</div><!-- /.col-sm-6 -->

					<div class="col-xs-6 col-sm-6">
						<a href="#" class="btn btn-linkedin btn-block"><i class="fa fa-linkedin"></i> LinkedIn</a>
					</div><!-- /.col-sm-6 -->
				</div><!-- /.row -->
			</form>
		</div><!-- /.col-sm-4 -->
	</div><!-- /.row -->
</div><!-- /.container -->