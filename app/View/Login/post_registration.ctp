<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active">
					<a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
						<strong>Tài khoản cá nhân</strong>
						<span>Tôi đang tìm kiếm một công việc</span>
					</a>
				</li>

				<li role="presentation">
					<a href="#company" aria-controls="company" role="tab" data-toggle="tab">
						<strong>Nhà tuyển dụng</strong>
						<span>Chúng tôi đang cần tuyển</span>
					</a>
				</li>
			</ul>

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="personal">
					<form action="" method="post" enctype="multipart/form-data">
						<input type="hidden" name="reg_type" value="personal">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="form-register-username">Tên đăng nhập</label>
									<input name="username" type="text" class="form-control" id="form-register-username">
								</div><!-- /.form-group -->

								<div class="form-group">
									<label for="form-register-email">E-mail</label>
									<input name="email" type="email" class="form-control" id="form-register-email">
								</div><!-- /.form-group -->

								<div class="form-group">
									<label for="form-register-password">Mật khẩu</label>
									<input name="passwd" type="password" class="form-control" id="form-register-password">
								</div><!-- /.form-group -->

								<div class="form-group">
									<label for="form-register-retype">Xác nhận mật khẩu</label>
									<input name="repasswd" type="password" class="form-control" id="form-register-retype">
								</div><!-- /.form-group -->
							</div><!-- /.col-* -->

							<div class="col-sm-6">
								<div class="row">
									<div class="form-group col-sm-6">
										<label for="form-register-first-name">Họ & Tên đệm</label>
										<input name="first_name" type="text" class="form-control" id="form-register-first-name">
									</div><!-- /.form-group -->

									<div class="form-group col-sm-6">
										<label for="form-login-surname">Tên</label>
										<input name="last_name" type="text" class="form-control" id="form-register-username">
									</div><!-- /.form-group -->
								</div><!-- /.row -->

								<div class="form-group">
									<label form="form-register-photo">Ảnh đại diện</label>
									<input name="avatar" type="file" name="form-register-photo" id="form-register-photo">
								</div><!-- /.form-group-->
							</div><!-- /.col-* -->
						</div><!-- /.row -->

						<div class="center">
							<div class="checkbox checkbox-info">
								<label><input name="agree" type="checkbox"> Tôi đã đọc và đồng ý với <a href="#">các điều khoản sử dụng</a></label>
							</div><!-- /.checkbox -->

							<button type="submit" class="btn btn-secondary">Tạo tài khoản</button>
						</div><!-- /.center -->
					</form>
				</div><!-- /.tab-pane -->

				<div role="tabpanel" class="tab-pane" id="company">
					<form action="" method="post" enctype="multipart/form-data">
						<input type="hidden" name="reg_type" value="company">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="form-register-company-username">Tên đăng nhập</label>
									<input name="username" type="text" class="form-control" id="form-register-company-username">
								</div><!-- /.form-group -->

								<div class="form-group">
									<label for="form-register-company-email">E-mail</label>
									<input name="email" type="email" class="form-control" id="form-register-company-email">
								</div><!-- /.form-group -->

								<div class="form-group">
									<label for="form-register-company-password">Mật khẩu</label>
									<input name="passwd" type="password" class="form-control" id="form-register-company-password">
								</div><!-- /.form-group -->

								<div class="form-group">
									<label for="form-register-company-retype">Xác nhận mật khẩu</label>
									<input name="repasswd" type="password" class="form-control" id="form-register-company-retype">
								</div><!-- /.form-group -->
							</div><!-- /.col-* -->

							<div class="col-sm-6">
								<div class="form-group">
									<label for="form-register-company-name">Tên công ty</label>
									<input name="company_name" type="text" class="form-control" id="form-register-company-name">
								</div><!-- /.form-group -->

								<div class="form-group">
									<label for="form-register-company-vat">Mã số thuế</label>
									<input name="company_tax_number" type="text" class="form-control" id="form-register-company-vat">
								</div><!-- /.form-group -->

								<div class="form-group">
									<label for="form-register-company-website">Website</label>
									<input name="homepage_url" type="text" class="form-control" id="form-register-company-website">
								</div><!-- /.form-group -->

								<div class="form-group">
									<label for="form-register-company-address-line">Địa chỉ</label>
									<input name="location" type="text" class="form-control" id="form-register-company-address-line">
								</div><!-- /.form-group -->
							</div><!-- /.col-* -->
						</div><!-- /.row -->

						<div class="center">
							<div class="checkbox checkbox-info">
								<label><input name="agree" type="checkbox"> Tôi đã đọc và đồng ý với <a href="#">các điều khoản sử dụng</a></label>
							</div><!-- /.checkbox -->

							<button type="submit" class="btn btn-secondary">Tạo tài khoản</button>
						</div><!-- /.center -->
					</form>
				</div><!-- /.tab-pane -->
			</div><!-- /.tab-content -->
		</div><!-- /.col-* -->
	</div><!-- /.row -->
</div><!-- /.container -->