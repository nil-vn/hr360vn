-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 18, 2016 at 03:43 PM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `danangjobs`
--

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `parent`) VALUES
(1, 'Electrical/Electronics', 'electrical-electronics', 0),
(2, 'Administrative/Clerical', 'administrative-clerical', 0),
(4, 'Finance/Investment', 'finance-investment', 0),
(5, 'Customer Service', 'customer-service', 0),
(6, 'Banking', 'banking', 0),
(7, 'Civil/Construction', 'civil-construction', 0),
(8, 'Auditing', 'auditing', 0),
(9, 'Accounting', 'accounting', 0),
(10, 'Architecture/Interior Design', 'architecture-interior-design', 0),
(11, 'Advertising/Promotion/PR', 'advertising-promotion-pr', 0),
(12, 'Human Resources', 'human-resources', 0),
(13, 'IT-Hardware/Networking', 'it-hardware-networking', 0),
(14, 'IT-Software/Programming', 'it-software-programming', 0),
(15, 'Internet/Online Media', 'internet-online-media', 0),
(16, 'Interpreter/Translator', 'interpreter-translator', 0),
(17, 'Marketing', 'marketing', 0),
(18, 'Mechanical', 'mechanical', 0),
(19, 'Production/Process', 'production-process', 0),
(20, 'Sales', 'sales', 0);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `career_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `logo_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `homepage_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_type` int(11) NOT NULL,
  `service_available_to` datetime NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jobs` int(11) NOT NULL DEFAULT '0',
  `employees` int(11) NOT NULL DEFAULT '0',
  `followers` int(11) NOT NULL DEFAULT '0',
  `rating` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rating_count` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `description`, `career_id`, `slug`, `status`, `logo_url`, `homepage_url`, `email`, `service_type`, `service_available_to`, `telephone`, `location`, `facebook_url`, `twitter_url`, `jobs`, `employees`, `followers`, `rating`, `rating_count`, `created`, `modified`) VALUES
(1, 'Twitter', '', 0, 'twitter', 1, 'http://128.199.151.194/assets/img/tmp/twitter.png', 'http://example.com', 'info@example.com', 2, '2016-11-18 11:22:14', '1-234-456-789', 'Denver, Colorado', NULL, NULL, 0, 1000, 99, '3', 50, '2016-11-18 11:14:33', '2016-11-18 11:22:14'),
(2, 'Airbnb', '', 2, 'airbnb', 1, 'http://128.199.151.194/assets/img/tmp/airbnb.png', 'http://example.com', 'info@example.com', 0, '2016-11-18 11:24:05', '1-234-456-789', 'New York City, New York', NULL, NULL, 0, 0, 0, '4', 35, '2016-11-18 11:14:33', '2016-11-18 11:24:05'),
(3, 'Dropbox', '', 1, 'dropbox', 1, 'http://128.199.151.194/assets/img/tmp/dropbox.png', 'http://example.com', 'info@example.com', 0, '2016-11-18 11:14:33', '1-234-456-789', 'Everton Street 231, San Francisco, California', NULL, NULL, 0, 0, 0, '5', 69, '2016-11-18 11:14:33', '2016-11-18 11:14:33'),
(4, 'Test', '', 0, 'test', 0, 'http://128.199.151.194/assets/img/tmp/dropbox.png', 'http://example.com', 'info@example.com', 0, '2016-11-18 11:25:05', '1-234-456-789', 'Everton Street 231, San Francisco, California', NULL, NULL, 0, 0, 0, NULL, 0, '2016-11-18 11:14:33', '2016-11-18 11:25:05'),
(5, 'Levy Inc', '', 0, 'levy-inc', 0, 'http://128.199.151.194/assets/img/tmp/dropbox.png', 'http://example.com', 'info@example.com', 0, '2016-11-18 11:25:27', '1-234-456-789', 'Everton Street 231, San Francisco, California', NULL, NULL, 0, 0, 0, NULL, 0, '2016-11-18 11:14:33', '2016-11-18 11:25:27'),
(6, 'Instagram', '', 1, 'instagram', 1, 'http://128.199.151.194/assets/img/tmp/instagram.png', 'http://example.com', 'info@example.com', 2, '2016-11-18 11:19:37', '1-234-456-789', 'Chicago, Michigan', NULL, NULL, 0, 0, 0, NULL, 0, '2016-11-18 11:19:37', '2016-11-18 11:19:37'),
(7, 'Facebook', '', 2, 'facebook', 1, 'http://128.199.151.194/assets/img/tmp/facebook.png', 'http://example.com', 'info@example.com', 2, '2016-11-18 11:20:27', '1-234-456-789', 'Philadelphia, Pennsylvania', NULL, NULL, 0, 0, 0, NULL, 0, '2016-11-18 11:20:27', '2016-11-18 11:20:27');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `job_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) DEFAULT NULL,
  `category_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary` int(11) NOT NULL,
  `available_to` datetime NOT NULL,
  `job_group_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_description` text COLLATE utf8_unicode_ci NOT NULL,
  `benefit` text COLLATE utf8_unicode_ci NOT NULL,
  `required` text COLLATE utf8_unicode_ci NOT NULL,
  `is_hot` int(11) NOT NULL,
  `hot_to` datetime DEFAULT NULL,
  `is_urgent` int(11) NOT NULL,
  `urgent_to` datetime DEFAULT NULL,
  `is_new` int(11) NOT NULL,
  `new_to` datetime DEFAULT NULL,
  `viewed` int(11) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `job_title`, `slug`, `company_id`, `location`, `type`, `category_id`, `salary`, `available_to`, `job_group_id`, `job_description`, `benefit`, `required`, `is_hot`, `hot_to`, `is_urgent`, `urgent_to`, `is_new`, `new_to`, `viewed`, `created`, `modified`) VALUES
(1, 'Senior Java Developer', 'senior-java-developer', 1, 'Da Nang', 3, '["13"]', 10, '2016-11-16 07:00:00', 'IJS001', 'Senior Java Developer', 'Senior Java Developer', 'Senior Java Developer', 1, NULL, 1, NULL, 1, NULL, 0, '2016-11-18 13:19:12', '2016-11-18 14:10:38'),
(2, 'UX/UI Senior Designer', 'ux-ui-senior-designer', 1, 'Da Nang', 1, '["2","11"]', 10, '2016-11-29 07:00:00', 'IFU001', 'UX/UI Senior Designer', 'UX/UI Senior Designer', 'UX/UI Senior Designer', 1, NULL, 0, NULL, 0, NULL, 0, '2016-11-18 13:19:12', '2016-11-18 14:10:54'),
(3, 'Junior Java Tester', 'junior-java-tester', 2, 'Da Nang', 1, '["8"]', 10, '2016-12-17 07:00:00', 'IJU116', 'Junior Java Tester', '<ul>\r\n				<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>\r\n				<li>Aliquam tincidunt mauris eu risus.</li>\r\n				<li>Vestibulum auctor dapibus neque.</li>\r\n				<li>Nunc dignissim risus id metus.</li>\r\n				<li>Cras ornare tristique elit.</li>\r\n				<li>Vivamus vestibulum nulla nec ante.</li>\r\n				<li>Praesent placerat risus quis eros.</li>\r\n				<li>Fusce pellentesque suscipit nibh.</li>\r\n				<li>Integer vitae libero ac risus egestas placerat.</li>\r\n				<li>Vestibulum commodo felis quis tortor.</li>\r\n				<li>Ut aliquam sollicitudin leo.</li>\r\n				<li>Cras iaculis ultricies nulla.</li>\r\n				<li>Donec quis dui at dolor tempor interdum.</li>\r\n				<li>Vivamus molestie gravida turpis.</li>\r\n				<li>Fusce lobortis lorem at ipsum semper sagittis.</li>\r\n				<li>Nam convallis pellentesque nisl.</li>\r\n				<li>Integer malesuada commodo nulla.</li>\r\n			</ul>', '<ul>\r\n				<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>\r\n				<li>Aliquam tincidunt mauris eu risus.</li>\r\n				<li>Vestibulum auctor dapibus neque.</li>\r\n				<li>Nunc dignissim risus id metus.</li>\r\n				<li>Cras ornare tristique elit.</li>\r\n				<li>Vivamus vestibulum nulla nec ante.</li>\r\n				<li>Praesent placerat risus quis eros.</li>\r\n				<li>Fusce pellentesque suscipit nibh.</li>\r\n				<li>Integer vitae libero ac risus egestas placerat.</li>\r\n				<li>Vestibulum commodo felis quis tortor.</li>\r\n				<li>Ut aliquam sollicitudin leo.</li>\r\n				<li>Cras iaculis ultricies nulla.</li>\r\n				<li>Donec quis dui at dolor tempor interdum.</li>\r\n				<li>Vivamus molestie gravida turpis.</li>\r\n				<li>Fusce lobortis lorem at ipsum semper sagittis.</li>\r\n				<li>Nam convallis pellentesque nisl.</li>\r\n				<li>Integer malesuada commodo nulla.</li>\r\n			</ul>', 0, NULL, 1, NULL, 0, NULL, 0, '2016-11-18 13:19:12', '2016-11-18 14:32:28'),
(4, 'Senior Frontend Developer', 'senior-frontend-developer', 7, 'Da Nang', 2, '["14"]', 10, '2017-07-29 07:00:00', 'IJU115', 'Senior Frontend Developer', 'Senior Frontend Developer', 'Senior Frontend Developer', 0, NULL, 1, NULL, 1, NULL, 0, '2016-11-18 13:19:12', '2016-11-18 14:10:15');

-- --------------------------------------------------------

--
-- Table structure for table `schema_migrations`
--

CREATE TABLE `schema_migrations` (
  `id` int(11) NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `schema_migrations`
--

INSERT INTO `schema_migrations` (`id`, `class`, `type`, `created`) VALUES
(1, 'InitMigrations', 'Migrations', '2016-11-18 10:55:54'),
(2, 'ConvertVersionToClassNames', 'Migrations', '2016-11-18 10:55:54'),
(3, 'IncreaseClassNameLength', 'Migrations', '2016-11-18 10:55:55'),
(4, 'CreateJobs', 'app', '2016-11-18 10:55:55'),
(5, 'CreateCompany', 'app', '2016-11-18 10:55:55'),
(6, 'CreateCareer', 'app', '2016-11-18 10:58:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `created`, `modified`) VALUES
(1, 'nil', '$2a$10$D1JUaoOwEdZgSGcbN7.JW.TASpNoQxs/s48KbhPCNUfK5N83F33Sq', 'admin', '2016-04-16 19:42:27', '2016-04-16 19:42:27'),
(2, 'staff', '$2a$10$cOI6QUrj8q1zVRCn9fbTOuGoa0zS60J1q5QnK2Vpq8jPwerIO/yp2', 'author', '2016-04-16 19:47:58', '2016-04-16 19:47:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schema_migrations`
--
ALTER TABLE `schema_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `schema_migrations`
--
ALTER TABLE `schema_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
